import 'package:flutter/material.dart';
import '../../widgets/shared_preference.dart';

class CheckBoxProvider with ChangeNotifier {
  bool _checkBoxSignUp = false;
  bool _checkBoxLogin = false;
  String _isSavedPassword;

  bool get checkBoxSignUp => _checkBoxSignUp;
  bool get checkBoxLogin => _checkBoxLogin;
  String get isSavePassword => _isSavedPassword;

  void checkBoxValueSignUp(bool val) {
    _checkBoxSignUp = val;
    notifyListeners();
  }

  void isSavedPassword() async {
    _isSavedPassword = await getStringPrefs('savePassword');
    _checkBoxLogin = _isSavedPassword == 'true' ? true : false;
    print('_checkBoxLogin==========>' + _checkBoxLogin.toString());
    notifyListeners();
  }

  void checkBoxValueLogin(bool val) async {
    _checkBoxLogin = val;
    setStringPrefs('savePassword', val.toString());
    val = await getStringPrefs('savePassword') == 'true' ? true : false;
    notifyListeners();
  }
}
