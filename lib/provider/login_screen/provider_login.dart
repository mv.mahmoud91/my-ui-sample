import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:rflutter_alert/rflutter_alert.dart';
import '../../models/api/login/profile_not_complete.dart';
import '../../models/api/login/sign_up_ok.dart';
import '../../models/api/set_new_member.dart';
import '../../widgets/utils.dart';
import '../../provider/dashboard_screen/dashboard_screen_provider.dart';
import '../../widgets/shared_preference.dart';
import '../../constants/constants.dart';
import '../../dio/ApiClient.dart';
import '../../dio/ApiConnection.dart';
import '../../models/api/login/login_model.dart';
import '../../models/api/login/login_wrong_info.dart';
import '../../models/api/recovery/recovery_error.dart';
import '../../models/api/recovery/recovery_success.dart';
import '../../screens/dashboard/dashboard_screen.dart';
import '../../screens/login/login_profile_screen.dart';

class LoginProvider with ChangeNotifier {
  int _pageIndex = 0;
  String encodedUserName,
      encodedPassword,
      decodedUserName,
      decodedPassword,
      _savePassword,
      _saveUserName,
      _companyName,
      _rules;

  bool _isSavedPassword, _isLoginStart;

  int get pageIndex => _pageIndex;

  String get saveUserName => _saveUserName;
  String get rules => _rules;
  String get savePassword => _savePassword;

  String get companyName => _companyName;

  bool get isSavedPassword => _isSavedPassword;

  bool get isLoginStart => _isLoginStart;

  void setRules(value) {
    _rules = value;
    notifyListeners();
  }

  void isSavedInput(bool val) {
    _isSavedPassword = val;
    notifyListeners();
  }

  void getCompanyName(String val) {
    _companyName = val;
    notifyListeners();
  }

  void isLoginStartInput(bool val) {
    _isLoginStart = val;
    notifyListeners();
  }

  void loadPassword() async {
    String username = await getStringPrefs('userName');
    String password = await getStringPrefs('password');
    decodedUserName = utf8.decode(base64Url.decode(username));
    decodedPassword = utf8.decode(base64Url.decode(password));
    _saveUserName = decodedUserName;
    _savePassword = decodedPassword;
    notifyListeners();
  }

  void signUpIndex() {
    _isLoginStart = false;
    _pageIndex = 0;
    notifyListeners();
  }

  void loginIndex() {
    _isLoginStart = false;
    _pageIndex = 1;
    notifyListeners();
  }

  void recoveryIndex() {
    _isLoginStart = false;
    _pageIndex = 2;
    notifyListeners();
  }

  void recoverySuccessIndex() {
    _isLoginStart = false;
    _pageIndex = 3;
    notifyListeners();
  }

  String _userName,
      _password,
      _recoverySuccessText,
      _showMobile,
      _recoveryUserName;
  String _token;
  bool _success = false;

  String get nationalCode => _userName;

  String get recoveryUserName => _recoveryUserName;

  String get showMobile => _showMobile;

  String get recoverySuccessText => _recoverySuccessText;

  bool get success => _success;

  String get phoneNumber => _password;

  String get token => _token;

  void phoneNumberInput(String val) {
    _password = val;
    notifyListeners();
  }

  void tokenSave(String val) {
    _token = val;
    notifyListeners();
  }

  void getRecoveryUserName(String val) {
    _recoveryUserName = val;
    notifyListeners();
  }

  void nationalCodeInput(String val) {
    _userName = val;
    notifyListeners();
  }

  String _nationalCodeSign, _phoneNumberSign;
  bool _checkedRules;
  bool _isConnected;

  String get nationalCodeSign => _nationalCodeSign;

  String get phoneNumberSign => _phoneNumberSign;

  bool get checkedRules => _checkedRules;

  bool get isConnected => _isConnected;

  void checkedRulesInput(bool checked) {
    _checkedRules = checked;
    notifyListeners();
  }

  void phoneNumberInputSign(String val) {
    _phoneNumberSign = val;
    notifyListeners();
  }

  void nationalCodeInputSign(String val) {
    _nationalCodeSign = val;
    notifyListeners();
  }

  void onSignUp(context) {
    showLoading(context);
    Future.delayed(new Duration(milliseconds: 200), () async {
      if (await checkConnectivity()) {
        callSignUpMember(
                '74-D4-35-5C-CD-F4',
                'IPhone6',
                '1.0.02',
                'xx',
                '1.0.02',
                '1000*200',
                '192.168.1.2',
                nationalCode,
                phoneNumber,
                true,
                11011)
            .then((response) {
          if (response.statusCode == 200) {
            final jsonResponse = jsonDecode(response.data);
            SetNewMember _data = SetNewMember.fromJson(jsonResponse);
            if (_data.result.returnValue == 0) {
              final SignUpOk signUpOk = SignUpOk.fromJson(jsonResponse);
              Navigator.pop(context);
              alertMessage(
                  context, signUpOk.result.returnValueMessage, COLOR_MESSAGE);
            } else {
              Navigator.pop(context);
              alertMessage(
                  context, _data.result.returnValueMessage, COLOR_MESSAGE);
            }
            print(_data.result.returnValueMessage);
          }
        }).catchError((error) {
          print(error);
        }).whenComplete(() {});
      } else if (await checkConnectivity() == false) {
        Navigator.pop(context);
        isConnect(context);
      }
    });
    notifyListeners();
  }

  void connect() async {
    if (await checkConnectivity()) {
      _isConnected = true;
    } else {
      _isConnected = false;
    }
    notifyListeners();
  }

  void onLoadingLogin(context) {
    showLoading(context);
    Future.delayed(new Duration(milliseconds: 1), () async {
      if (await checkConnectivity()) {
        callLogin('74-D4-35-5C-CD-F4', 'IPhone6', '1.0.02', 'xx', '1.0.02',
                '1000*200', '192.168.1.2', 2, 3, _userName, _password)
            .then((response) {
          if (response.statusCode == 200) {
            final jsonResponse = jsonDecode(response.data);
            print(jsonResponse);
            Map<String, dynamic> map = jsonResponse;
            if (map['Result']['ReturnValue'] == 1000) {
              ProfileNotComplete profileNotComplete =
                  ProfileNotComplete.fromJson(jsonResponse);
              tokenSave(profileNotComplete.token);
              setStringPrefs('token', profileNotComplete.token);
              print(token);
              Navigator.pushNamed(context, LoginProfileScreen.routName);
            } else if (map['Result']['ReturnValue'] == 4) {
              LoginWrongInfo loginWrongInfo =
                  LoginWrongInfo.fromJson(jsonResponse);
              Navigator.pop(context);
              alertMessage(context, loginWrongInfo.result.returnValueMessage,
                  COLOR_MESSAGE);
            } else {
              loginSuccess(jsonResponse, context);
            }
          }
        }).catchError((error) {
          print(error);
        }).whenComplete(() {
          print('salam');
        });
      } else if (await checkConnectivity() == false) {
        Navigator.pop(context);
        isConnect(context);
      }
    });

    notifyListeners();
  }

  void loginSuccess(jsonResponse, context) {
    LoginModel _data = LoginModel.fromJson(jsonResponse);
    encodedUserName = base64Url.encode(utf8.encode(_userName));
    if (isSavedPassword != null && isSavedPassword) {
      encodedUserName = base64Url.encode(utf8.encode(_userName));
      encodedPassword = base64Url.encode(utf8.encode(_password));
      setStringPrefs('userName', encodedUserName);
      setStringPrefs('password', encodedPassword);
      _saveUserName = _userName;
      _savePassword = _password;
    } else {
      setStringPrefs('userName', encodedUserName);
      setStringPrefs('password', '');
    }
    ProviderDashBoardScreen provider = ProviderDashBoardScreen();

    _token = _data.token;
    setStringPrefs('token', _token);
    provider.setToken(_token);
    print(provider.token);
    print(token);
    Navigator.pushReplacementNamed(context, DashboardScreen.routName);
  }

  void onFingerPrint(context, String userName, String password) {
    showLoading(context);
    Future.delayed(new Duration(milliseconds: 1), () async {
      if (await checkConnectivity()) {
        callLogin('74-D4-35-5C-CD-F4', 'IPhone6', '1.0.02', 'xx', '1.0.02',
                '1000*200', '192.168.1.2', 2, 3, userName, password)
            .then((response) {
          if (response.statusCode == 200) {
            final jsonResponse = jsonDecode(response.data);
            print(jsonResponse);
            LoginModel _data = LoginModel.fromJson(jsonResponse);
            _token = _data.token;
            setStringPrefs('token', _data.token);
            print(token);

            Navigator.pushReplacementNamed(context, DashboardScreen.routName);
          }
        }).catchError((error) {
          print(error);
        }).whenComplete(() {});
      } else if (await checkConnectivity() == false) {
        Navigator.pop(context);
        isConnect(context);
      }
    });

    notifyListeners();
  }

  void onLoadingRecovery(context) {
    showLoading(context);
    Future.delayed(new Duration(milliseconds: 1), () async {
      if (await checkConnectivity()) {
        callRecovery('74-D4-35-5C-CD-F4', 'IPhone6', '1.0.02', 'xx', '1.0.02',
                '1000*200', '192.168.1.2', 2, 3, _recoveryUserName)
            .then((response) {
          if (response.statusCode == 200) {
            final jsonResponse = jsonDecode(response.data);
            print(jsonResponse);
            Map<String, dynamic> map = jsonResponse;
            if (map['Result']['ReturnValue'] == -100) {
              RecoveryError recoveryError =
                  RecoveryError.fromJson(jsonResponse);
              Navigator.pop(context);
              alertMessage(context, recoveryError.result.returnValueMessage,
                  COLOR_MESSAGE);
            } else if (map['Result']['ReturnValue'] == 0) {
              RecoverySuccess recoverySuccess =
                  RecoverySuccess.fromJson(jsonResponse);
              _recoverySuccessText =
                  recoverySuccess.result.returnValueMessage.toString();
              _showMobile = recoverySuccess.member.mobile.toString();
              Navigator.pop(context);
              recoverySuccessIndex();
            }
          }
        }).catchError((error) {
          print(error);
        }).whenComplete(() {});
      } else if (await checkConnectivity() == false) {
        Navigator.pop(context);
        isConnect(context);
      }
    });

    notifyListeners();
  }

  void isConnect(context) {
    Alert(
        closeFunction: () {},
        context: context,
        image: Image.asset(
          'assets/images/danger.png',
          width: 50.0,
          height: 50.0,
          fit: BoxFit.contain,
          color: Colors.red,
        ),
        style: AlertStyle(
          isOverlayTapDismiss: false,
          isCloseButton: false,
          titleStyle: TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
        ),
        title:
            'اتصال به اینترنت برقرار نمی باشد. لطفا اتصال اینترنت گوشی خود را بررسی نمایید.',
        buttons: [
          DialogButton(
            color: COLOR_MESSAGE,
            onPressed: () {
              connect();
              if (isConnected) {
                Navigator.pop(context);
              }
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width * .2,
              child: FittedBox(
                child: Text(
                  "تلاش مجدد",
                  style: TextStyle(color: Colors.white, fontSize: 13),
                ),
              ),
            ),
          )
        ]).show();
    notifyListeners();
  }
}
