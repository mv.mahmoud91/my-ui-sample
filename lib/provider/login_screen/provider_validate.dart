import 'package:flutter/material.dart';

class ValidateProvider with ChangeNotifier {
  bool _correct = true;

  bool get correct => _correct;

  void isCorrect() {
    _correct = true;
    notifyListeners();
  }

  void notCorrect() {
    _correct = false;
    notifyListeners();
  }
}
