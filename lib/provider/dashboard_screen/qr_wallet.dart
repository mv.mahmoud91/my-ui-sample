import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:sample_company/models/api/qr_page/get_bank_cards.dart';
import '../../constants/constants.dart';
import '../../dio/ApiClient.dart';
import '../../dio/ApiConnection.dart';
import '../../models/api/main/token_expird.dart';
import '../../models/api/qr_page/qr_code_accept.dart';
import '../../screens/dashboard/pay_screen.dart';
import '../../widgets/shared_preference.dart';
import '../../widgets/utils.dart';

class QrWallet with ChangeNotifier {
  String _qrCode, _token;
  QrCodeAccept _qrCodeAccept;
  GetBankCard _bankCards;
  bool _isConnected = true;

  String get qrCode => _qrCode;
  QrCodeAccept get qrCodeAccept => _qrCodeAccept;
  bool get isConnected => _isConnected;
  String get token => _token;
  GetBankCard get bankCards => _bankCards;

  getQrCodeAccept(value) {
    _qrCodeAccept = value;
    notifyListeners();
  }

  getBankCards(value) {
    _bankCards = value;
    notifyListeners();
  }

  getToken() async {
    return _token = await getStringPrefs('token');
  }

  setQrCode(value) {
    _qrCode = value;
    notifyListeners();
  }

  void qrLoad(context, QRViewController controller) {
    getToken();
    connect();
    showLoading(context);
    Future.delayed(Duration(milliseconds: 1), () async {
      if (await checkConnectivity()) {
        callQrCode('74-D4-35-5C-CD-F4', 'IPhone6', '1.0.02', 'xx', '1.0.02',
                '1000*200', '192.168.1.2', qrCode, token)
            .then((response) {
          if (response.statusCode == 200) {
            final jsonResponse = jsonDecode(response.data);
            Map<String, dynamic> map = jsonResponse;
            if (map['Result']['ReturnValue'] == 0) {
              final _codeAccept = QrCodeAccept.fromJson(jsonResponse);
              getQrCodeAccept(_codeAccept);
              Navigator.pop(context);
              Navigator.pushNamed(context, PayScreen.routName);
            } else if (map['Result']['ReturnValue'] == 3000 ||
                map['Result']['ReturnValue'] == 4000) {
              TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
              Navigator.pop(context);
              errorOrTokenExpirdAlert(context,
                  tokenExpird.result.returnValueMessage, COLOR_MESSAGE);
            } else {
              TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
              Navigator.pop(context);
              qrAlertMessage(context, tokenExpird.result.returnValueMessage,
                  COLOR_MESSAGE, controller);
            }
          }
        });
        callDashBoard('74-D4-35-5C-CD-F4', 'IPhone6', '1.0.02', 'xx', '1.0.02',
                '1000*200', '192.168.1.2', token, 'GetBankCards')
            .then((response) async {
          if (response.statusCode == 200) {
            final jsonResponse = jsonDecode(response.data);
            print(jsonResponse);
            Map<String, dynamic> map = jsonResponse;
            if (map['Result']['ReturnValue'] == 3000 ||
                map['Result']['ReturnValue'] == 4000 ||
                map['Result']['ReturnValue'] == -100) {
              TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
              Navigator.pop(context);
              errorOrTokenExpirdAlert(context,
                  tokenExpird.result.returnValueMessage, COLOR_MESSAGE);
            } else if (map['Result']['ReturnValue'] == 0) {
              GetBankCard getBankCard = GetBankCard.fromJson(jsonResponse);
              getBankCards(getBankCard);
              print('s');
            } else {
              print(map['Result']['ReturnValue']);
            }
          }
        }).catchError((error) {
          print(error);
        }).whenComplete(() {});
      }
    });
  }

  void isConnect(context) {
    Alert(
        closeFunction: () {},
        context: context,
        image: Image.asset(
          'assets/images/danger.png',
          width: 50.0,
          height: 50.0,
          fit: BoxFit.contain,
          color: Colors.red,
        ),
        style: AlertStyle(
          isOverlayTapDismiss: false,
          isCloseButton: false,
          titleStyle: TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
        ),
        title:
            'اتصال به اینترنت برقرار نمی باشد. لطفا اتصال اینترنت گوشی خود را بررسی نمایید.',
        buttons: [
          DialogButton(
            color: COLOR_MESSAGE,
            onPressed: () {
              connect();
              if (isConnected) {
                Navigator.pop(context);
              }
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width * .2,
              child: FittedBox(
                child: Text(
                  "تلاش مجدد",
                  style: TextStyle(color: Colors.white, fontSize: 13),
                ),
              ),
            ),
          )
        ]).show();
    notifyListeners();
  }

  void connect() async {
    if (await checkConnectivity()) {
      _isConnected = true;
    } else {
      _isConnected = false;
    }
    notifyListeners();
  }
}
