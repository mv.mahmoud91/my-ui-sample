import 'package:flutter/material.dart';

import '../../models/api/main/dashboard/get_dashboard.dart';
import '../../models/api/main/chargeinventorypage/get_payment_gate_way_info.dart';

class ProviderDashBoardScreen with ChangeNotifier {
  List<String> _slider = [];
  List<PaymentTerminal> _bank = [];
  MemberInfo _memberInfo;
  String _money = '0';
  int _bnbIndex = 2;
  String _token, _minCharge, _maxCharge;
  bool _isConnected;
  GetDashboard getDashboard;

  int get bnbIndex => _bnbIndex;

  String get token => _token;

  String get money => _money;

  String get minCharge => _minCharge;

  String get maxCharge => _maxCharge;

  List<String> get slider => _slider;

  List<PaymentTerminal> get bank => _bank;
  MemberInfo get memberInfo => _memberInfo;

  bool get isConnected => _isConnected;

  void setBnbIndex(int index) {
    _bnbIndex = index;
    notifyListeners();
  }

  void exitDashboard() {
    _bnbIndex = 2;
    notifyListeners();
  }

  void setSlider(List<String> slide) {
    _slider = slide;
    notifyListeners();
  }

  void setMemberInfo(MemberInfo info) {
    _memberInfo = info;
    notifyListeners();
  }

  void setMoney(String value) {
    _money = value;
    notifyListeners();
  }

  void setToken(String token) {
    _token = token;
    notifyListeners();
  }

// void onDashboard(context) {
//   showDialog(
//     context: context,
//     barrierDismissible: false,
//     builder: (BuildContext context) {
//       return Dialog(
//         child: Container(
//           padding: const EdgeInsets.all(10.0),
//           width: MediaQuery.of(context).size.width * .6,
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             mainAxisSize: MainAxisSize.min,
//             children: [
//               Text(
//                 "لطفا چند لحظه صبر کنید ...".trim(),
//                 style: TextStyle(fontWeight: FontWeight.normal),
//               ),
//               CircularProgressIndicator(),
//             ],
//           ),
//         ),
//       );
//     },
//   );
//   Future.delayed(new Duration(milliseconds: 200), () async {
//     if (await checkConnectivity()) {
//       _token = await getStringPrefs('token');
//
//       callDashBoard('74-D4-35-5C-CD-F4', 'IPhone6', '1.0.02', 'xx', '1.0.02',
//               '1000*200', '192.168.1.2', token, 'GetDashboard')
//           .then((response) async {
//         if (response.statusCode == 200) {
//           final jsonResponse = jsonDecode(response.data);
//           print(jsonResponse);
//           Map<String, dynamic> map = jsonResponse;
//           if (map['Result']['ReturnValue'] == 3000 ||
//               map['Result']['ReturnValue'] == 4000 ||
//               map['Result']['ReturnValue'] == -100) {
//             TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
//             Navigator.pop(context);
//             Alert(
//                 closeFunction: () {},
//                 context: context,
//                 image: Image.asset(
//                   'assets/images/danger.png',
//                   width: 50.0,
//                   height: 50.0,
//                   fit: BoxFit.contain,
//                   color: Colors.red,
//                 ),
//                 style: AlertStyle(
//                   isOverlayTapDismiss: false,
//                   isCloseButton: false,
//                   titleStyle: TextStyle(
//                       fontSize: 14.0, fontWeight: FontWeight.normal),
//                 ),
//                 title: tokenExpird.result.returnValueMessage,
//                 buttons: [
//                   DialogButton(
//                     color: Constants.COLOR_MESSAGE,
//                     onPressed: () => Navigator.pushReplacementNamed(
//                         context, LoginScreen.routName),
//                     child: Text(
//                       "بستن",
//                       style: TextStyle(color: Colors.white, fontSize: 14),
//                     ),
//                   )
//                 ]).show();
//           } else if (map['Result']['ReturnValue'] == 0) {
//             GetDashboard getDashboard = GetDashboard.fromJson(jsonResponse);
//             setMoney(getDashboard.account.remain.toString());
//             print(money);
//             getData() async {
//               for (var item in getDashboard.slider) {
//                 _slider.add(item.photoUrl);
//               }
//             }
//
//             await getData();
//             Navigator.pop(context);
//           } else {
//             print(map['Result']['ReturnValue']);
//           }
//         }
//       }).catchError((error) {
//         print(error);
//       }).whenComplete(() {});
//     } else if (await checkConnectivity() == false) {
//       Navigator.pop(context);
//       isConnect(context);
//     }
//   });
//
//   notifyListeners();
// }
//
// void isConnect(context) {
//   Alert(
//       closeFunction: () {},
//       context: context,
//       image: Image.asset(
//         'assets/images/danger.png',
//         width: 50.0,
//         height: 50.0,
//         fit: BoxFit.contain,
//         color: Colors.red,
//       ),
//       style: AlertStyle(
//         isOverlayTapDismiss: false,
//         isCloseButton: false,
//         titleStyle: TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
//       ),
//       title:
//           'اتصال به اینترنت برقرار نمی باشد. لطفا اتصال اینترنت گوشی خود را بررسی نمایید.',
//       buttons: [
//         DialogButton(
//           color: Constants.COLOR_MESSAGE,
//           onPressed: () {
//             connect();
//             if (isConnected) {
//               Navigator.pop(context);
//             }
//           },
//           child: SizedBox(
//             width: MediaQuery.of(context).size.width * .2,
//             child: FittedBox(
//               child: Text(
//                 "تلاش مجدد",
//                 style: TextStyle(color: Colors.white, fontSize: 13),
//               ),
//             ),
//           ),
//         )
//       ]).show();
//   notifyListeners();
// }
//
// void connect() async {
//   if (await checkConnectivity()) {
//     _isConnected = true;
//   } else {
//     _isConnected = false;
//   }
//   notifyListeners();
// }
}
