import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:sample_company/provider/dashboard_screen/dashboard_screen_provider.dart';
import '../../widgets/login/custom_show_dialog.dart';
import '../../models/api/main/chargeinventorypage/charge_url.dart';
import '../login/login_profile_screen.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../dio/ApiClient.dart';
import '../../dio/ApiConnection.dart';
import '../../models/api/main/chargeinventorypage/get_payment_gate_way_info.dart';
import '../../models/api/main/token_expird.dart';
import '../../widgets/shared_preference.dart';
import '../../constants/constants.dart';
import '../../widgets/chargewallet/textfield_widget_charge.dart';
import '../../widgets/chargewallet/charge_wallet_select_amount.dart';
import '../../widgets/utils.dart';

class ChargeWalletScreen extends StatefulWidget {
  static const routName = '/ChargeWalletScreen';

  @override
  _ChargeWalletScreenState createState() => _ChargeWalletScreenState();
}

class _ChargeWalletScreenState extends State<ChargeWalletScreen> {
  String rules = '';
  TextEditingController textAmountCharge = TextEditingController();
  PaymentTerminal terminalValues;
  String token, minCharge = '0', maxCharge = '0';
  bool isConnected;
  List<PaymentTerminal> paymentTerminalBanks = [];
  int _currentIndex = 0;
  String dataValue = '';
  int chargeValue = 0;
  List<String> _amountCharge = [
    '50000',
    '100000',
    '500000',
    '1000000',
  ];
  List<bool> isSelected = [
    false,
    false,
    false,
    false,
  ];

  _items() {
    List<DropdownMenuItem<PaymentTerminal>> paymentTerminal = [];
    paymentTerminalBanks.forEach((bankItem) {
      Uint8List bytes = base64Decode(bankItem.terminalLogo);
      DropdownMenuItem<PaymentTerminal> item =
          DropdownMenuItem<PaymentTerminal>(
        value: bankItem,
        child: Row(
          children: [
            Image.memory(
              bytes,
              width: 30.0,
              height: 30.0,
            ),
            Text(
              bankItem.terminalName,
              style: TextStyle(fontSize: 14.0),
            ),
          ],
        ),
      );
      paymentTerminal.add(item);
    });
    return paymentTerminal;
  }

  getToken() async {
    return token = await getStringPrefs('token');
  }

  @override
  void initState() {
    getToken();
    Future.delayed(Duration.zero).then((_) {
      onChargeInventory();
    });
    super.initState();
  }

  @override
  void dispose() {
    textAmountCharge?.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Provider.of<ProviderDashBoardScreen>(context, listen: false)
            .setBnbIndex(2);
        return false;
      },
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40.0, vertical: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: GridView.builder(
                  shrinkWrap: true,
                  padding: EdgeInsets.all(0.0),
                  itemCount: _amountCharge.length,
                  itemBuilder: (ctx, index) {
                    return GestureDetector(
                      onTap: () => selectAmount(index),
                      child: ChargeSelectAmountContainer(
                        rial: _amountCharge[index],
                        isSelected: isSelected[index],
                      ),
                    );
                  },
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, childAspectRatio: 2.5),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Form(
                key: _formKey,
                child: TextFieldWidgetCharge(
                  submitted: _charge,
                  text: 'مبلغ دلخواه :',
                  textEditingController: textAmountCharge,
                  textFunction: _validateCharge,
                  textInputFormatter: FilteringTextInputFormatter.digitsOnly,
                  length: 14,
                  hintText: 'مبلغ به ریال',
                  obSecure: false,
                  textInputType: TextInputType.number,
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                width: MediaQuery.of(context).size.width * .6,
                child: FittedBox(
                  child: Text(
                    'مبلغ باید بین' +
                        ' ' +
                        minCharge.toString().toMoneyFormat() +
                        ' ' +
                        'تا' +
                        ' ' +
                        maxCharge.toString().toMoneyFormat() +
                        ' ' +
                        'ریال' +
                        ' ' +
                        'باشد.',
                    style: TextStyle(color: Colors.red[900]),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                'درگاه بانک :',
                style: acceptStyle(context),
              ),
              // SizedBox(
              //   width: 50.0,
              //   height: 40.0,
              //   child:
              // ),
              SizedBox(
                height: 10.0,
              ),
              DropdownButtonFormField(
                decoration: InputDecoration(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                ),
                onChanged: (selected) {
                  terminalValues = selected;
                },
                value: terminalValues,
                items: _items(),
                isDense: true,
                itemHeight: 60.0,
                elevation: 2,
              ),
              SizedBox(
                width: double.infinity,
                child: Builder(
                  builder: (context) => RaisedButton(
                    padding: EdgeInsets.symmetric(vertical: 5.0),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    color: COLOR_MESSAGE,
                    onPressed: () {
                      validateAmount();
                    },
                    child: Text(
                      'شارژ',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Center(
                child: GestureDetector(
                  onTap: () => _inShowCustomDialog(context, rules),
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10.0, vertical: 5.0),
                    decoration: BoxDecoration(
                        color: COLOR_MESSAGE,
                        borderRadius: BorderRadius.circular(30.0)),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          width: 20.0,
                          height: 20.0,
                          child: SvgPicture.asset(
                            'assets/images/tickets.svg',
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * .3,
                          child: FittedBox(
                            child: Text(
                              'قوانین و مقررات شارژ کیف پول',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onChargeUrl() {
    showLoading(context);
    Future.delayed(new Duration(milliseconds: 1), () async {
      if (await checkConnectivity()) {
        callPayment(
          '74-D4-35-5C-CD-F4',
          'IPhone6',
          '1.0.02',
          'xx',
          '1.0.02',
          '1000*200',
          '192.168.1.2',
          1,
          terminalValues.terminalId,
          chargeValue,
          token,
        ).then((response) {
          if (response.statusCode == 200) {
            final jsonResponse = jsonDecode(response.data);
            print(jsonResponse);
            Map<String, dynamic> map = jsonResponse;
            if (map['Result']['ReturnValue'] == 3000 ||
                map['Result']['ReturnValue'] == 4000 ||
                map['Result']['ReturnValue'] == -100) {
              TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
              Navigator.pop(context);
              alertMessage(context, tokenExpird.result.returnValueMessage,
                  COLOR_MESSAGE);
            } else if (map['Result']['ReturnValue'] == 0) {
              ChargeUrl chargeUrl = ChargeUrl.fromJson(jsonResponse);
              Navigator.pop(context);
              Navigator.pop(context);
              _launchURL(chargeUrl.paymentTransactionToken.urlRouter);
            } else {
              TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
              Alert(
                  closeFunction: () {},
                  context: context,
                  image: Image.asset(
                    'assets/images/danger.png',
                    width: 50.0,
                    height: 50.0,
                    fit: BoxFit.contain,
                    color: Colors.red,
                  ),
                  style: AlertStyle(
                    isOverlayTapDismiss: false,
                    isCloseButton: false,
                    titleStyle: TextStyle(
                        fontSize: 14.0, fontWeight: FontWeight.normal),
                  ),
                  title: tokenExpird.result.returnValueMessage,
                  buttons: [
                    DialogButton(
                      color: COLOR_MESSAGE,
                      onPressed: () {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                      child: Text(
                        "بستن",
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    )
                  ]).show();
              print(map['Result']['ReturnValue']);
            }
          }
        }).catchError((error) {
          print(error);
        }).whenComplete(() {});
      } else if (await checkConnectivity() == false) {
        Navigator.pop(context);
        isConnect(context);
      }
    });
  }

  void onChargeInventory() {
    showLoading(context);
    Future.delayed(new Duration(milliseconds: 1), () async {
      if (await checkConnectivity()) {
        callDashBoard('74-D4-35-5C-CD-F4', 'IPhone6', '1.0.02', 'xx', '1.0.02',
                '1000*200', '192.168.1.2', token, 'GetPaymentGatewayInfo')
            .then((response) {
          if (response.statusCode == 200) {
            final jsonResponse = jsonDecode(response.data);
            print(jsonResponse);
            Map<String, dynamic> map = jsonResponse;
            if (map['Result']['ReturnValue'] == 3000 ||
                map['Result']['ReturnValue'] == 4000 ||
                map['Result']['ReturnValue'] == -100) {
              TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
              Navigator.pop(context);
              errorOrTokenExpirdAlert(context,
                  tokenExpird.result.returnValueMessage, COLOR_MESSAGE);
            } else if (map['Result']['ReturnValue'] == 0) {
              setState(() {
                GetPaymentGateWayInfo gateWayInfo =
                    GetPaymentGateWayInfo.fromJson(jsonResponse);
                minCharge = gateWayInfo.paymentTerminal[0].minCharge.toString();
                maxCharge = gateWayInfo.paymentTerminal[0].maxCharge.toString();
                for (var item in gateWayInfo.paymentTerminal) {
                  paymentTerminalBanks.add(item);
                  terminalValues = gateWayInfo.paymentTerminal[0];
                }
                rules = gateWayInfo.description;
              });
              Navigator.pop(context);
            } else {
              print(map['Result']['ReturnValue']);
            }
          }
        }).catchError((error) {
          print(error);
        }).whenComplete(() {});
      } else if (await checkConnectivity() == false) {
        Navigator.pop(context);
        isConnect(context);
      }
    });
  }

  void isConnect(context) {
    Alert(
        closeFunction: () {},
        context: context,
        image: Image.asset(
          'assets/images/danger.png',
          width: 50.0,
          height: 50.0,
          fit: BoxFit.contain,
          color: Colors.red,
        ),
        style: AlertStyle(
          isOverlayTapDismiss: false,
          isCloseButton: false,
          titleStyle: TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
        ),
        title:
            'اتصال به اینترنت برقرار نمی باشد. لطفا اتصال اینترنت گوشی خود را بررسی نمایید.',
        buttons: [
          DialogButton(
            color: COLOR_MESSAGE,
            onPressed: () {
              connect();
              if (isConnected) {
                Navigator.pop(context);
              }
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width * .2,
              child: FittedBox(
                child: Text(
                  "تلاش مجدد",
                  style: TextStyle(color: Colors.white, fontSize: 13),
                ),
              ),
            ),
          )
        ]).show();
  }

  void connect() async {
    if (await checkConnectivity()) {
      setState(() {
        isConnected = true;
      });
    } else {
      setState(() {
        isConnected = false;
      });
    }
  }

  String _validateCharge(String value) {
    if (value.isEmpty && dataValue.isEmpty) {
      return 'لطفا مبلغ را وارد نمایید';
    } else if (validateAmountCharge(value) == false && dataValue.isEmpty) {
      return 'مبلغ باید بین' +
          ' ' +
          minCharge.toMoneyFormat() +
          ' ' +
          'تا' +
          ' ' +
          maxCharge.toMoneyFormat() +
          ' ' +
          'ریال باشد';
    } else {
      return null;
    }
  }

  void _charge(String value) {
    if (value.length > 0) {
      setState(() {
        for (int i = 0; i < isSelected.length; i++) {
          isSelected[i] = false;
          dataValue = '';
        }
      });
      print(isSelected);
    } else {}
  }

  void validateAmount() {
    String value = '';
    if (_formKey.currentState.validate()) {
      if (dataValue.isEmpty) {
        value = textAmountCharge.text;
        chargeValue = int.parse(value.replaceAll(RegExp(','), ''));
        print('============>' + chargeValue.toString());
      } else {
        chargeValue = int.parse(dataValue.replaceAll(RegExp(','), ''));
        print('============>' + chargeValue.toString());
      }
      Alert(
          closeFunction: () {},
          context: context,
          image: Image.asset(
            'assets/images/danger.png',
            width: 50.0,
            height: 50.0,
            fit: BoxFit.contain,
            color: Colors.red,
          ),
          style: AlertStyle(
            isOverlayTapDismiss: false,
            isCloseButton: false,
            titleStyle:
                TextStyle(fontSize: 13.0, fontWeight: FontWeight.normal),
          ),
          title: 'آیا از صحت اطلاعات وارد شده اطمینان دارید ؟',
          content: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: AcceptDialog(
                      text: 'مبلغ : ',
                      controller:
                          chargeValue.toString().toMoneyFormat() + ' ' + 'ریال',
                      context: context),
                ),
                AcceptDialog(
                    text: 'درگاه بانک : ',
                    controller: terminalValues.terminalName,
                    context: context),
              ],
            ),
          ),
          buttons: [
            DialogButton(
              color: COLOR_MESSAGE,
              onPressed: () => Navigator.pop(context),
              child: Text(
                "خیر",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
            ),
            DialogButton(
              color: COLOR_MESSAGE,
              onPressed: () => onChargeUrl(),
              child: Text(
                "بله",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
            ),
          ]).show();
    } else {}
  }

  void _launchURL(String urlAddress) async {
    if (await canLaunch(urlAddress)) {
      await launch(urlAddress);
    } else {
      throw 'Could not launch $urlAddress';
    }
  }

  selectAmount(int index) {
    setState(() {
      _currentIndex = index;
      if (textAmountCharge.text.length > 0) {
        for (int i = 0; i < isSelected.length; i++) {
          isSelected[i] = false;
        }
      } else {
        for (int i = 0; i < isSelected.length; i++) {
          if (index == _currentIndex) {
            isSelected[i] = false;
            isSelected[_currentIndex] = true;
            dataValue = _amountCharge[index];
          }
        }
      }
    });
  }

  void _inShowCustomDialog(BuildContext context, String rules) {
    showGeneralDialog(
      barrierColor: Colors.black54,
      transitionDuration: Duration(milliseconds: 200),
      barrierDismissible: true,
      barrierLabel: '',
      context: context,
      transitionBuilder: (context, a1, a2, widget) {
        final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
        var size = MediaQuery.of(context).size;
        return Transform(
          transform: Matrix4.translationValues(0.0, curvedValue * 200, 0.0),
          child: AModal(
            child: Container(
              width: size.width * .99,
              height: size.height * .5,
              child: SingleChildScrollView(
                child: Text(
                  rules,
                  style: loginStyle(context),
                ),
              ),
            ),
          ),
        );
      },
      pageBuilder: (BuildContext context, Animation animation,
          Animation secondaryAnimation) {
        return;
      },
    );
  }
}
