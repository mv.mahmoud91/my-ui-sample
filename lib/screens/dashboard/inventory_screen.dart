import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample_company/provider/dashboard_screen/dashboard_screen_provider.dart';
import '../../provider/login_screen/provider_login.dart';
import '../../widgets/shared_preference.dart';

class InventoryScreen extends StatefulWidget {
  @override
  _InventoryScreenState createState() => _InventoryScreenState();
}

class _InventoryScreenState extends State<InventoryScreen> {
  bool description = false;
  String userName;
  String password;
  @override
  void initState() {
    final LoginProvider provider =
        Provider.of<LoginProvider>(context, listen: false);
    super.initState();
    userName = provider.nationalCode;
    password = provider.phoneNumber;
    loadShared();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Provider.of<ProviderDashBoardScreen>(context, listen: false)
            .setBnbIndex(2);
        return false;
      },
      child: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('موجودی'),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('قابلیت ورود با اثر انگشت'),
                  CupertinoSwitch(
                    onChanged: (bool value) {
                      setState(() {
                        description = value;
                        if (value) {
                          print('true');
                          String encodedUserName =
                              base64Url.encode(utf8.encode(userName));
                          String encodedPassword =
                              base64Url.encode(utf8.encode(password));
                          setStringPrefs('fingerUserName', encodedUserName);
                          setStringPrefs('fingerPassword', encodedPassword);
                          setStringPrefs('fingerStatus', 'true');
                        } else {
                          print('false');
                          setStringPrefs('fingerUserName', 'false');
                          setStringPrefs('fingerPassword', 'false');
                          setStringPrefs('fingerStatus', 'false');
                        }
                      });
                    },
                    value: description,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  loadShared() async {
    String fingerStatus = await getStringPrefs('fingerStatus');
    if (fingerStatus == 'true') {
      setState(() {
        description = true;
      });
    } else {
      setState(() {
        description = false;
      });
    }
  }
}
