import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:sample_company/screens/login/login_screen.dart';
import '../../constants/constants.dart';
import '../../dio/ApiClient.dart';
import '../../dio/ApiConnection.dart';
import '../../models/api/main/dashboard/get_dashboard.dart';
import '../../models/api/main/token_expird.dart';
import '../../provider/dashboard_screen/dashboard_screen_provider.dart';
import '../../widgets/dashboard/dashboard_top_slider.dart';
import '../../widgets/dashboard/dashboard_widget.dart';
import '../../widgets/shared_preference.dart';
import '../../widgets/utils.dart';

class MainDashboardScreen extends StatefulWidget {
  @override
  _MainDashboardScreenState createState() => _MainDashboardScreenState();
}

class _MainDashboardScreenState extends State<MainDashboardScreen> {
  GetDashboard getDashboard;
  List<String> _slider = [];
  String _money = '0';
  String token = '';
  bool isConnected;
  Future<bool> _onWillPop() async {
    return (await Alert(
        closeFunction: () {},
        context: context,
        image: Image.asset(
          'assets/images/danger.png',
          width: 30.0,
          height: 30.0,
          fit: BoxFit.contain,
          color: Colors.red,
        ),
        style: AlertStyle(
          isOverlayTapDismiss: false,
          isCloseButton: false,
          titleStyle: TextStyle(fontSize: 11.0, fontWeight: FontWeight.normal),
        ),
        title: 'آیا مایل به خروج از حساب کاربری خود هستید ؟',
        buttons: [
          DialogButton(
            color: COLOR_MESSAGE,
            onPressed: () => Navigator.of(context).pop(false),
            child: Text(
              "خیر",
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
          ),
          DialogButton(
            color: COLOR_MESSAGE,
            onPressed: () {
              Navigator.pushNamedAndRemoveUntil(
                  context, LoginScreen.routName, (route) => false);
            },
            child: Text(
              "بله",
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
          ),
        ]).show());
  }

  void getToken() async {
    token = await getStringPrefs('token');
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero).then((_) {
      getToken();
      onDashboard();
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Column(
        children: [
          TopSlider(
            slider: _slider,
            money: _money,
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: DashBoardWidget(),
            ),
          ),
        ],
      ),
    );
  }

  void onDashboard() async {
    showLoading(context);
    if (await checkConnectivity()) {
      callDashBoard('74-D4-35-5C-CD-F4', 'IPhone6', '1.0.02', 'xx', '1.0.02',
              '1000*200', '192.168.1.2', token, 'GetDashboard')
          .then((response) async {
        if (response.statusCode == 200) {
          final jsonResponse = jsonDecode(response.data);
          print(jsonResponse);
          Map<String, dynamic> map = jsonResponse;
          if (map['Result']['ReturnValue'] == 3000 ||
              map['Result']['ReturnValue'] == 4000 ||
              map['Result']['ReturnValue'] == -100) {
            TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
            Navigator.pop(context);
            errorOrTokenExpirdAlert(
                context, tokenExpird.result.returnValueMessage, COLOR_MESSAGE);
          } else if (map['Result']['ReturnValue'] == 0) {
            dashboardSuccess(jsonResponse);
          } else {
            print(map['Result']['ReturnValue']);
          }
        }
      }).catchError((error) {
        print(error);
      }).whenComplete(() {});
    } else if (await checkConnectivity() == false) {
      Navigator.pop(context);
      connectionDialog(context, COLOR_MESSAGE, connect, isConnected);
    }
  }

  void dashboardSuccess(jsonResponse) {
    getDashboard = GetDashboard.fromJson(jsonResponse);
    setStringPrefs('qrCode', getDashboard.memberInfo.barcodeImage);
    final provider =
        Provider.of<ProviderDashBoardScreen>(context, listen: false);
    provider.setMoney(getDashboard.account.remain.toString());
    provider.setMemberInfo(getDashboard.memberInfo);
    print('============>>>>>' + provider.money.toString());
    for (var item in getDashboard.slider) {
      _slider.add(item.photoUrl);
    }

    provider.setSlider(_slider);
    provider.setMoney(getDashboard.account.remain.toString());
    setState(() {});
    print('==========>' + provider.slider.toString());
    _money = getDashboard.account.remain.toString();
    Navigator.pop(context);
  }

  void connect() async {
    if (await checkConnectivity()) {
      setState(() {
        isConnected = true;
      });
    } else {
      setState(() {
        isConnected = false;
      });
    }
  }
}
