import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import '../../provider/dashboard_screen/dashboard_screen_provider.dart';
import '../../provider/dashboard_screen/qr_wallet.dart';
import '../../constants/constants.dart';
import '../../widgets/qrcode/textfield_widget_amount.dart';

class QrWalletScreen extends StatefulWidget {
  static const routName = '/QrWalletScreen';

  @override
  _QrWalletScreenState createState() => _QrWalletScreenState();
}

class _QrWalletScreenState extends State<QrWalletScreen> {
  GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController controller;
  TextEditingController _payTextController;
  var _qrText = '';

  String _money = '1000';

  get qrContext => _qrText;
  @override
  void initState() {
    final provider =
        Provider.of<ProviderDashBoardScreen>(context, listen: false);
    super.initState();
    _money = provider.money;
    _payTextController = TextEditingController();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('========>build');
    var size = MediaQuery.of(context).size;
    return Consumer<QrWallet>(builder: (ctx, provider, _) {
      void onQRViewCreated(QRViewController qrViewController) {
        this.controller = qrViewController;
        controller.scannedDataStream.listen((scanData) {
          _qrText = scanData;
          if (_qrText.length > 0) {
            controller.pauseCamera();
            provider.setQrCode(_qrText);
            provider.qrLoad(context, controller);
          } else {
            print('qrText ========> nothing');
            // controller.resumeCamera();
          }
        });
      }

      return WillPopScope(
        onWillPop: () async {
          Provider.of<ProviderDashBoardScreen>(context, listen: false)
              .setBnbIndex(2);
          return false;
        },
        child: SingleChildScrollView(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 40.0, vertical: 20.0),
            child: Column(
              children: [
                TextFieldWidgetQrCode(
                  money: _money,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  color: Colors.grey,
                  width: double.infinity,
                  height: size.height * .4,
                  child: Stack(
                    children: [
                      QRView(
                        key: qrKey,
                        onQRViewCreated: onQRViewCreated,
                        overlay: QrScannerOverlayShape(
                          borderColor: Colors.red,
                          borderRadius: 10,
                          borderLength: 30,
                          borderWidth: 10,
                          cutOutSize: 300,
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Container(
                          margin: const EdgeInsets.all(10.0),
                          width: 50.0,
                          height: 50.0,
                          decoration: BoxDecoration(
                            color: COLOR_MESSAGE,
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: IconButton(
                            onPressed: () {
                              controller?.resumeCamera();
                            },
                            icon: Icon(
                              Icons.refresh,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  height: 35.0,
                  child: Row(
                    children: [
                      Text(
                        'پرداخت با شناسه :',
                        style: TextStyle(fontSize: 11.0),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Expanded(
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          controller: _payTextController,
                          style: TextStyle(fontSize: 12),
                          textDirection: TextDirection.ltr,
                          decoration: InputDecoration(
                            contentPadding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                SizedBox(
                  width: double.infinity,
                  child: QrCodeButton(
                    qrCode: _payTextController.text,
                    controller: controller,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}

class QrCodeButton extends StatelessWidget {
  QrCodeButton({@required this.qrCode, @required this.controller});
  final String qrCode;
  final QRViewController controller;
  @override
  Widget build(BuildContext qrCodeContext) {
    return Consumer<QrWallet>(
      builder: (qrCodeContext, value, _) {
        return RaisedButton(
          onPressed: () {
            controller.pauseCamera();
            value.setQrCode(qrCode);
            value.qrLoad(qrCodeContext, controller);
          },
          child: Text(
            'تایید',
            style:
                TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
          ),
          color: COLOR_MESSAGE,
        );
      },
    );
  }
}
