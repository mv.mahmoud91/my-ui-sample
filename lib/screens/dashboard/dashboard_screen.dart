import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import '../../constants/constants.dart';
import 'bills_screen.dart';
import 'inventory_screen.dart';
import 'main_dashboard_screen.dart';
import '../../widgets/shared_preference.dart';
import '../../provider/dashboard_screen/dashboard_screen_provider.dart';
import 'charge_wallet_screen.dart';
import 'qr_wallet_screen.dart';
import '../../widgets/dashboard/dashboard_bnb_widget.dart';

class DashboardScreen extends StatefulWidget {
  static const routName = '/DashboardScreen';

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final String assetName = 'assets/images/';
  List<Widget> bodyWidget = [
    ChargeWalletScreen(),
    InventoryScreen(),
    MainDashboardScreen(),
    BillsScreen(),
    QrWalletScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ProviderDashBoardScreen>(context).bnbIndex;
    print('=================>build dashboard');

    return SafeArea(
      child: Scaffold(
        extendBody: true,
        appBar: AppBar(
          backgroundColor: Color(0xFF1c8914),
          title: Text(
            provider == 0
                ? 'شارژ کیف پول'
                : provider == 1
                    ? 'موجودی'
                    : provider == 2
                        ? 'شاندیز من'
                        : provider == 3
                            ? 'صورتحساب'
                            : 'پرداختی بدون کارت',
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w200),
          ),
          leading: IconButton(
            icon: Icon(
              Icons.menu,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
          actions: [
            provider == 2
                ? Row(
                    children: [
                      IconButton(
                        icon: SvgPicture.asset(
                          assetName + 'bell.svg',
                          color: Colors.white,
                          width: 25.0,
                          height: 25.0,
                        ),
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: SvgPicture.asset(
                          assetName + 'qr-code.svg',
                          color: Colors.white,
                          width: 25.0,
                          height: 25.0,
                        ),
                        onPressed: () async {
                          String qrCode = await getStringPrefs('qrCode');
                          Uint8List profile = base64Decode(qrCode);
                          final provider = Provider.of<ProviderDashBoardScreen>(
                              context,
                              listen: false);
                          showAlert(context, profile, provider);
                        },
                      ),
                    ],
                  )
                : IconButton(
                    onPressed: () {
                      Provider.of<ProviderDashBoardScreen>(context,
                              listen: false)
                          .setBnbIndex(2);
                    },
                    icon: Icon(Icons.arrow_forward_ios),
                  ),
          ],
        ),
        body: bodyWidget[provider],
        // MainDashboardScreen(imageUrl: imageUrl),
        bottomNavigationBar: BNBWidget(),
      ),
    );
  }

  void showAlert(BuildContext context, Uint8List profile, final provider) {
    showDialog(
      useSafeArea: true,
      context: context,
      builder: (context) => AlertDialog(
        content: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.memory(
                profile,
                // height: double.maxFinite,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                provider.memberInfo.genderName +
                    ' ' +
                    provider.memberInfo.firstName +
                    ' ' +
                    provider.memberInfo.lastName,
                style: acceptStyle(context),
              ),
              Text(
                'شناسه مشتری : ' + provider.memberInfo.customerId.toString(),
                style: acceptStyle2(context),
              ),
              RaisedButton(
                onPressed: () => Navigator.pop(context),
                color: COLOR_MESSAGE,
                child: Text(
                  'بستن',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
