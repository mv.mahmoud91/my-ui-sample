import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample_company/provider/dashboard_screen/dashboard_screen_provider.dart';

class BillsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Provider.of<ProviderDashBoardScreen>(context, listen: false)
            .setBnbIndex(2);
        return false;
      },
      child: Container(
        width: double.infinity,
        height: double.maxFinite,
        color: Colors.white,
        child: Center(
          child: FittedBox(child: Text('صورتحساب')),
        ),
      ),
    );
  }
}
