import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:sample_company/dio/ApiClient.dart';
import 'package:sample_company/dio/ApiConnection.dart';
import 'package:sample_company/models/api/main/chargeinventorypage/get_payment_gate_way_info.dart';
import 'package:sample_company/models/api/main/token_expird.dart';
import 'package:sample_company/models/api/qr_page/get_bank_cards.dart';
import 'package:sample_company/models/api/qr_page/qr_code_accept.dart';
import 'package:sample_company/models/api/qr_page/qr_payment_sucess.dart';
import 'package:sample_company/provider/dashboard_screen/qr_wallet.dart';
import 'package:sample_company/screens/login/login_profile_screen.dart';
import 'package:sample_company/widgets/shared_preference.dart';
import 'package:sample_company/widgets/utils.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../models/api/main/dashboard/get_dashboard.dart';
import '../../provider/dashboard_screen/dashboard_screen_provider.dart';
import '../../widgets/chargewallet/charge_wallet_select_amount.dart';
import '../../widgets/chargewallet/textfield_widget_charge.dart';
import '../../constants/constants.dart';

enum PayType { bank, bag }

class PayScreen extends StatefulWidget {
  static const routName = '/PayScreen';

  @override
  _PayScreenState createState() => _PayScreenState();
}

class _PayScreenState extends State<PayScreen> with TickerProviderStateMixin {
  AnimationController _controller;
  PayType _payType = PayType.bag;
  PaymentTerminal terminalValues;
  QrCodeAccept _qrCodeAccept;
  MemberInfo _memberInfo;
  GetBankCard _bankCards;
  TextEditingController textAmountCharge = TextEditingController();
  int _currentIndex = 0;
  String dataValue = '', token = '';
  String _money = '0';
  bool _haveMoney = false;
  List<PaymentTerminal> paymentTerminalBanks = [];
  bool isConnected;
  double chargeValue = 0;

  getToken() async {
    return token = await getStringPrefs('token');
  }

  _items() {
    List<DropdownMenuItem<PaymentTerminal>> paymentTerminal = [];
    paymentTerminalBanks.forEach((bankItem) {
      Uint8List bytes = base64Decode(bankItem.terminalLogo);
      DropdownMenuItem<PaymentTerminal> item =
          DropdownMenuItem<PaymentTerminal>(
        value: bankItem,
        child: Row(
          children: [
            Image.memory(
              bytes,
              width: 30.0,
              height: 30.0,
            ),
            Text(
              bankItem.terminalName,
              style: TextStyle(fontSize: 14.0),
            ),
          ],
        ),
      );
      paymentTerminal.add(item);
    });
    return paymentTerminal;
  }

  @override
  void initState() {
    final ProviderDashBoardScreen provider =
        Provider.of<ProviderDashBoardScreen>(context, listen: false);
    final QrWallet qrWallet = Provider.of<QrWallet>(context, listen: false);
    getToken();
    Future.delayed(Duration.zero).then((_) {
      onChargeInventory();
    });
    super.initState();
    _controller = AnimationController(vsync: this);
    _memberInfo = provider.memberInfo;
    // _bankCards = qrWallet.bankCards;
    _money = provider.money;
    _qrCodeAccept = qrWallet.qrCodeAccept;

    print(_bankCards);
    print(_money);
    setState(() {
      if (_money.isEmpty || double.parse(_money) <= 0) {
        _haveMoney = false;
      } else {
        _haveMoney = true;
      }
    });
  }

  List<String> _amountCharge = [
    '10000',
    '20000',
    '40000',
    '50000',
  ];
  List<bool> isSelected = [
    false,
    false,
    false,
    false,
  ];

  @override
  void dispose() {
    _controller?.dispose();
    textAmountCharge?.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      backgroundColor: COLOR_MESSAGE,
      title: Text(
        'پرداخت',
        style: TextStyle(fontWeight: FontWeight.normal, fontSize: 15.0),
      ),
      leading: SizedBox.shrink(),
      actions: [
        IconButton(
          icon: Icon(Icons.arrow_forward_ios),
          onPressed: () {
            Navigator.pop(context);
            Provider.of<ProviderDashBoardScreen>(context, listen: false)
                .setBnbIndex(4);
          },
        )
      ],
    );
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context);

        return false;
      },
      child: Scaffold(
        appBar: appBar,
        body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 40.0, vertical: 5.0),
          child: Container(
            // height: size.height - sizeArea,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Center(
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: 5.0),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey[400]),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 15.0),
                        width: 150.0,
                        height: 180.0,
                        child: LayoutBuilder(
                          builder: (BuildContext context, BoxConstraints box) {
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SvgPicture.asset(
                                  _memberInfo.gender
                                      ? 'assets/images/user-male.svg'
                                      : 'assets/images/user-female.svg',
                                  color: Colors.black,
                                  width: box.maxWidth * .55,
                                ),
                                Column(
                                  children: [
                                    Container(
                                      width: box.maxWidth * .28,
                                      child: FittedBox(
                                        child: Text(
                                            _qrCodeAccept.payment.brandName),
                                      ),
                                    ),
                                    Container(
                                      width: box.maxWidth * .98,
                                      child: FittedBox(
                                        child: Text('نام پذیرنده :' +
                                            ' ' +
                                            _qrCodeAccept.payment.brandAgent),
                                      ),
                                    ),
                                    Container(
                                      width: box.maxWidth * .8,
                                      child: FittedBox(
                                        child: Text('شناسه پذیرنده :' +
                                            ' ' +
                                            _qrCodeAccept.payment.qRcodeOut),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                    ),
                    Container(
                      child: GridView.builder(
                        shrinkWrap: true,
                        padding: EdgeInsets.all(0.0),
                        itemCount: _amountCharge.length,
                        itemBuilder: (ctx, index) {
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                _currentIndex = index;
                                if (textAmountCharge.text.length > 0) {
                                  for (int i = 0; i < isSelected.length; i++) {
                                    isSelected[i] = false;
                                  }
                                } else {
                                  for (int i = 0; i < isSelected.length; i++) {
                                    if (index == _currentIndex) {
                                      isSelected[i] = false;
                                      isSelected[_currentIndex] = true;
                                      dataValue = _amountCharge[index];
                                      if (double.parse(dataValue) >
                                              double.parse(_money) &&
                                          _payType == PayType.bag) {
                                        _haveMoney = false;
                                      } else {
                                        _haveMoney = true;
                                      }
                                    }
                                  }
                                }
                              });
                            },
                            child: ChargeSelectAmountContainer(
                              rial: _amountCharge[index],
                              isSelected: isSelected[index],
                            ),
                          );
                        },
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 2,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Form(
                      key: _formKey,
                      child: TextFieldWidgetCharge(
                        submitted: _charge,
                        text: 'مبلغ دلخواه :',
                        textEditingController: textAmountCharge,
                        textFunction: _validateCharge,
                        textInputFormatter:
                            FilteringTextInputFormatter.digitsOnly,
                        length: 14,
                        hintText: 'مبلغ به ریال',
                        obSecure: false,
                        textInputType: TextInputType.number,
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'نحوه پرداخت :',
                      style: chargeStyle(context),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 5.0),
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Radio<PayType>(
                            value: PayType.bank,
                            groupValue: _payType,
                            onChanged: (PayType value) {
                              setState(() {
                                _payType = value;
                                _haveMoney = true;
                              });
                            },
                          ),
                          Expanded(
                            child: DropdownButtonFormField(
                              onChanged: (selected) {
                                terminalValues = selected;
                              },
                              value: terminalValues,
                              items: _items(),
                              isDense: true,
                              itemHeight: 60.0,
                              elevation: 2,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 5.0),
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Radio<PayType>(
                                value: PayType.bag,
                                groupValue: _payType,
                                onChanged: (PayType value) {
                                  setState(() {
                                    _validPay();
                                    _payType = value;
                                  });
                                },
                              ),
                              SizedBox(
                                width: 20.0,
                                height: 20.0,
                                child: SvgPicture.asset(
                                    'assets/images/kif-pul.svg'),
                              ),
                              SizedBox(width: 10.0),
                              Text(
                                'کیف پول',
                                style: qrStyle(context),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                _money.toMoneyFormat() + ' ' + 'ریال',
                                style: chargeStyle(context),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        color: COLOR_MESSAGE,
                        elevation: 0.0,
                        onPressed: _haveMoney
                            ? () {
                                validateAmount();
                              }
                            : null,
                        child: Text(
                          'پرداخت',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14.0,
                              fontWeight: FontWeight.normal),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _validPay() {
    if (_money.isNotEmpty && double.parse(_money) <= 0 ||
        textAmountCharge.text.isNotEmpty &&
            double.parse(textAmountCharge.text) > double.parse(_money) ||
        dataValue.isNotEmpty &&
            double.parse(dataValue) > double.parse(_money)) {
      _haveMoney = false;
    } else {
      _haveMoney = true;
    }
  }

  void validateAmount() {
    // print(
    //     '==================================>' + _bankCards.bankCard[0].cardNo);
    String value = '';
    if (_formKey.currentState.validate()) {
      if (dataValue.isEmpty) {
        value = textAmountCharge.text;
        chargeValue = double.parse(value.replaceAll(RegExp(','), ''));
        print('============>' + chargeValue.toString());
      } else {
        chargeValue = double.parse(dataValue.replaceAll(RegExp(','), ''));
        print('============>' + chargeValue.toString());
      }
      Alert(
          closeFunction: () {},
          context: context,
          image: Image.asset(
            'assets/images/danger.png',
            width: 50.0,
            height: 50.0,
            fit: BoxFit.contain,
            color: Colors.red,
          ),
          style: AlertStyle(
            isOverlayTapDismiss: false,
            isCloseButton: false,
            titleStyle:
                TextStyle(fontSize: 11.0, fontWeight: FontWeight.normal),
          ),
          title: 'آیا از صحت اطلاعات وارد شده اطمینان دارید ؟',
          content: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: AcceptDialog(
                      text: 'مبلغ : ',
                      controller:
                          chargeValue.toString().toMoneyFormat() + ' ' + 'ریال',
                      context: context),
                ),
                AcceptDialog(
                    text: 'درگاه بانک : ',
                    controller: terminalValues.terminalName,
                    context: context),
              ],
            ),
          ),
          buttons: [
            DialogButton(
              color: COLOR_MESSAGE,
              onPressed: () => Navigator.pop(context),
              child: Text(
                "خیر",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
            ),
            DialogButton(
              color: COLOR_MESSAGE,
              onPressed: () {
                Navigator.pop(context);
                if (_bankCards.bankCard.isEmpty) {
                  alertMessage(
                      context, 'کارت تعریف شده ای وجود ندارد', COLOR_MESSAGE);
                } else {
                  if (_payType == PayType.bag) {
                    onChargeUrl('666', 1);
                  } else {
                    onChargeUrl(_bankCards.bankCard[0].cardNo, 2);
                  }
                }
              },
              child: Text(
                "بله",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
            ),
          ]).show();
    } else {}
  }

  void onChargeUrl(String cardNo, int paymentType) {
    showLoading(context);
    Future.delayed(new Duration(milliseconds: 1), () async {
      if (await checkConnectivity()) {
        qrPayment(
          '74-D4-35-5C-CD-F4',
          'IPhone6',
          '1.0.02',
          'xx',
          '1.0.02',
          '1000*200',
          '192.168.1.2',
          _qrCodeAccept.payment.qRcodeOut,
          chargeValue,
          cardNo,
          paymentType,
          terminalValues.terminalId,
          token,
        ).then((response) {
          if (response.statusCode == 200) {
            final jsonResponse = jsonDecode(response.data);
            print(jsonResponse);
            Map<String, dynamic> map = jsonResponse;
            if (map['Result']['ReturnValue'] == 3000 ||
                map['Result']['ReturnValue'] == 4000 ||
                map['Result']['ReturnValue'] == -100) {
              TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
              Navigator.pop(context);
              alertMessage(context, tokenExpird.result.returnValueMessage,
                  COLOR_MESSAGE);
            } else if (map['Result']['ReturnValue'] == 0) {
              QrPaymentSuccess success =
                  QrPaymentSuccess.fromJson(jsonResponse);
              if (success.paymentTransactionToken.urlRouter == null) {
                _controller.reset();
                Alert(
                    closeFunction: () {},
                    context: context,
                    image: Lottie.asset(
                      'assets/animations/actionSuccess.json',
                      controller: _controller,
                      onLoaded: (composition) {
                        _controller
                          ..duration = composition.duration
                          ..forward();
                      },
                      repeat: false,
                      width: 75.0,
                      height: 75.0,
                      fit: BoxFit.fill,
                      animate: true,
                    ),
                    style: AlertStyle(
                      isOverlayTapDismiss: false,
                      isCloseButton: false,
                      titleStyle: TextStyle(
                          fontSize: 14.0, fontWeight: FontWeight.normal),
                    ),
                    title: 'پرداخت با موفقیت انجام شد',
                    buttons: [
                      DialogButton(
                        color: COLOR_MESSAGE,
                        onPressed: () => Navigator.pop(context),
                        child: Text(
                          "بستن",
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                      )
                    ]).show();
              } else {
                Navigator.pop(context);
                _launchURL(success.paymentTransactionToken.urlRouter);
              }
            } else {
              TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
              Navigator.pop(context);
              Alert(
                  closeFunction: () {},
                  context: context,
                  image: Image.asset(
                    'assets/images/danger.png',
                    width: 50.0,
                    height: 50.0,
                    fit: BoxFit.contain,
                    color: Colors.red,
                  ),
                  style: AlertStyle(
                    isOverlayTapDismiss: false,
                    isCloseButton: false,
                    titleStyle: TextStyle(
                        fontSize: 14.0, fontWeight: FontWeight.normal),
                  ),
                  title: tokenExpird.result.returnValueMessage,
                  buttons: [
                    DialogButton(
                      color: COLOR_MESSAGE,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "بستن",
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    )
                  ]).show();
              print(map['Result']['ReturnValue']);
            }
          }
        }).catchError((error) {
          print(error);
        }).whenComplete(() {});
      } else if (await checkConnectivity() == false) {
        Navigator.pop(context);
        isConnect(context);
      }
    });
  }

  void _launchURL(String urlAddress) async {
    Navigator.pop(context);
    if (await canLaunch(urlAddress)) {
      await launch(urlAddress);
    } else {
      throw 'Could not launch $urlAddress';
    }
  }

  void onChargeInventory() {
    showLoading(context);
    Future.delayed(new Duration(milliseconds: 1), () async {
      if (await checkConnectivity()) {
        callDashBoard('74-D4-35-5C-CD-F4', 'IPhone6', '1.0.02', 'xx', '1.0.02',
                '1000*200', '192.168.1.2', token, 'GetPaymentGatewayInfo')
            .then((response) {
          if (response.statusCode == 200) {
            final jsonResponse = jsonDecode(response.data);
            print(jsonResponse);
            Map<String, dynamic> map = jsonResponse;
            if (map['Result']['ReturnValue'] == 3000 ||
                map['Result']['ReturnValue'] == 4000 ||
                map['Result']['ReturnValue'] == -100) {
              TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
              Navigator.pop(context);
              errorOrTokenExpirdAlert(context,
                  tokenExpird.result.returnValueMessage, COLOR_MESSAGE);
            } else if (map['Result']['ReturnValue'] == 0) {
              final QrWallet qrWallet =
                  Provider.of<QrWallet>(context, listen: false);
              setState(() {
                _bankCards = qrWallet.bankCards;
                _qrCodeAccept = qrWallet.qrCodeAccept;
                GetPaymentGateWayInfo gateWayInfo =
                    GetPaymentGateWayInfo.fromJson(jsonResponse);
                for (var item in gateWayInfo.paymentTerminal) {
                  paymentTerminalBanks.add(item);
                  terminalValues = gateWayInfo.paymentTerminal[0];
                }
              });
              Navigator.pop(context);
            } else {
              print(map['Result']['ReturnValue']);
            }
          }
        }).catchError((error) {
          print(error);
        }).whenComplete(() {});
      } else if (await checkConnectivity() == false) {
        Navigator.pop(context);
        isConnect(context);
      }
    });
  }

  void isConnect(context) {
    Alert(
        closeFunction: () {},
        context: context,
        image: Image.asset(
          'assets/images/danger.png',
          width: 50.0,
          height: 50.0,
          fit: BoxFit.contain,
          color: Colors.red,
        ),
        style: AlertStyle(
          isOverlayTapDismiss: false,
          isCloseButton: false,
          titleStyle: TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
        ),
        title:
            'اتصال به اینترنت برقرار نمی باشد. لطفا اتصال اینترنت گوشی خود را بررسی نمایید.',
        buttons: [
          DialogButton(
            color: COLOR_MESSAGE,
            onPressed: () {
              connect();
              if (isConnected) {
                Navigator.pop(context);
              }
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width * .2,
              child: FittedBox(
                child: Text(
                  "تلاش مجدد",
                  style: TextStyle(color: Colors.white, fontSize: 13),
                ),
              ),
            ),
          )
        ]).show();
  }

  void connect() async {
    if (await checkConnectivity()) {
      setState(() {
        isConnected = true;
      });
    } else {
      setState(() {
        isConnected = false;
      });
    }
  }

  void _charge(String value) {
    if (value.length > 0) {
      setState(() {
        for (int i = 0; i < isSelected.length; i++) {
          isSelected[i] = false;
          dataValue = '';
        }
      });
      print(isSelected);
    } else {}
  }

  String _validateCharge(String value) {
    if (value.isEmpty && dataValue.isEmpty) {
      return 'لطفا مبلغ را وارد نمایید';
    } else if (validateAmountCharge(value) == false && dataValue.isEmpty) {
      return 'مبلغ وارد شده معتبر نمی باشد';
    } else {
      return null;
    }
  }
}
