import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import '../models/api/get_rules.dart';
import '../widgets/shared_preference.dart';
import '../provider/login_screen/provider_checkbox.dart';
import '../provider/login_screen/provider_login.dart';
import 'login/login_screen.dart';
import '../constants/constants.dart';
import '../dio/ApiConnection.dart';
import '../models/api/loading_app.dart';
import '../widgets/utils.dart';
import '../dio/ApiClient.dart';

class SplashScreen extends StatefulWidget {
  static const routName = '/SplashScreen';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isConnect = true;
  bool isLoading = false;
  String rules;
  String companyName;

  @override
  void initState() {
    super.initState();
    _connect();

    Future.delayed(Duration(seconds: 5), () {
      _onCheckApp();
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Align(
                alignment: Alignment.center,
                child: Lottie.asset(
                  'assets/animations/splash.json',
                  repeat: false,
                  width: size.width * .8,
                  fit: BoxFit.fill,
                ),
              ),
            ],
          ),
          Positioned(
            bottom: 0,
            right: 0,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              width: size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  !isConnect
                      ? Column(
                          children: [
                            !isLoading
                                ? SvgPicture.asset(
                                    'assets/images/error.svg',
                                    color: COLOR_MESSAGE,
                                  )
                                : SizedBox.shrink(),
                            SizedBox(
                              height: 10.0,
                            ),
                            !isLoading
                                ? Text(
                                    INTERNET_OFF_MESSAGE,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: COLOR_MESSAGE, fontSize: 12),
                                  )
                                : SizedBox.shrink(),
                            !isLoading
                                ? RaisedButton(
                                    onPressed: () {
                                      _onCheckApp(retry: true);
                                    },
                                    color: COLOR_MESSAGE,
                                    splashColor: COLOR_MESSAGE.withGreen(100),
                                    child: Text(
                                      INTERNET_AGAIN_TRY,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12,
                                          fontWeight: FontWeight.normal),
                                    ),
                                  )
                                : CircularProgressIndicator(),
                          ],
                        )
                      : SizedBox.shrink(),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    'نسخه : 1.0.0'.toFarsi(),
                    style: TextStyle(
                        color: COLOR_MESSAGE,
                        fontWeight: FontWeight.normal,
                        fontSize: 11.0),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _connect() async {
    if (await checkConnectivity()) {
      Provider.of<CheckBoxProvider>(context, listen: false).isSavedPassword();
      Provider.of<LoginProvider>(context, listen: false).loadPassword();
      bool save = await getStringPrefs('savePassword') == 'true' ? true : false;
      Provider.of<LoginProvider>(context, listen: false)
          .isLoginStartInput(save);

      setState(() {
        isConnect = true;
      });
    } else {
      setState(() {
        isConnect = false;
      });
    }
  }

  void _onCheckApp({retry = false}) async {
    if (await checkConnectivity()) {
      setState(() {
        isLoading = retry;
        isConnect = !retry;
      });

      callCheckApp('[{"ShopeID":-1}]', '74-D4-35-5C-CD-F4', 'IPhone6', '1.0.02',
              'xx', '1.0.02', '1000*200', '192.168.1.2', 'GetInitialize')
          .then((response) {
        if (response.statusCode == 200) {
          setState(() {
            final jsonResponse = jsonDecode(response.data);
            LoadingApp _data = LoadingApp.fromJson(jsonResponse);
            print(_data.initialize[0].companyName);
            companyName = _data.initialize[0].companyName;
          });
          final provider = Provider.of<LoginProvider>(context, listen: false);
          provider.getCompanyName(companyName);
          Navigator.pushReplacementNamed(context, LoginScreen.routName);
        }
      }).catchError((error) {
        print(error);
      }).whenComplete(() {});

      callCheckApp('[{"ShopeID":-1}]', '74-D4-35-5C-CD-F4', 'IPhone6', '1.0.02',
              'xx', '1.0.02', '1000*200', '192.168.1.2', 'GetRules')
          .then((response) {
        if (response.statusCode == 200) {
          final jsonResponse = jsonDecode(response.data);
          GetRules _data = GetRules.fromJson(jsonResponse);
          rules = _data.rules.context;
          final provider = Provider.of<LoginProvider>(context, listen: false);
          provider.setRules(rules);
          Navigator.pushReplacementNamed(context, LoginScreen.routName);
        }
      }).catchError((error) {
        print(error);
      }).whenComplete(() {});
    } else if (await checkConnectivity() == false) {
      print('اتصال اینترنت برقرار نیست');
      setState(() {
        isConnect = false;
        isLoading = false;
      });
    }
  }
}
