import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../provider/login_screen/provider_login.dart';
import '../../constants/constants.dart';
import '../../widgets/login/bottom_form_widget.dart';

class LoginScreen extends StatefulWidget {
  static const routName = '/LoginScreen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<LoginProvider>(context, listen: false);
    final String companyName = provider.companyName;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: ListView(
          children: [
            Column(
              children: [
                BgLoginTop(),
                LoginBottomForm(
                  companyName: companyName,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class BgLoginTop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: size.height * .25,
      color: COLOR_MESSAGE,
      child: Image.asset(
        'assets/images/bg_login_top_1.png',
        width: double.infinity,
        fit: BoxFit.fill,
      ),
    );
  }
}
