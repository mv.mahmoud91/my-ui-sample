import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:persian_datepicker/persian_datepicker.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import '../../dio/ApiClient.dart';
import '../../dio/ApiConnection.dart';
import '../../models/api/login/profile_ok.dart';
import '../../models/api/main/token_expird.dart';
import '../../provider/login_screen/provider_login.dart';
import '../dashboard/dashboard_screen.dart';
import '../../constants/constants.dart';
import 'login_screen.dart';
import '../../widgets/login/text_field_widget.dart';

enum SingingCharacter { man, woman }

class LoginProfileScreen extends StatefulWidget {
  LoginProfileScreen({this.companyName});

  final String companyName;
  static const routName = '/LoginProfileScreen';

  @override
  _LoginProfileScreenState createState() => _LoginProfileScreenState();
}

class _LoginProfileScreenState extends State<LoginProfileScreen> {
  final TextEditingController textEditingController = TextEditingController();
  LoginProvider loginProvider;
  bool isConnected = true;
  PersianDatePickerWidget persianDatePicker;
  TextEditingController controllerName;
  TextEditingController controllerFamily;
  String companyName;
  SingingCharacter _gender = SingingCharacter.man;
  final Map<String, dynamic> userProfile = {};
  String token;

  @override
  void initState() {
    loginProvider = Provider.of<LoginProvider>(context, listen: false);
    controllerName = TextEditingController();
    controllerFamily = TextEditingController();
    persianDatePicker = PersianDatePicker(
      controller: textEditingController,
    ).init();
    companyName = loginProvider.companyName;
    token = loginProvider.token;
    super.initState();
    print(token);
  }

  @override
  void dispose() {
    controllerName.dispose();
    controllerFamily.dispose();
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('rebuild');
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: COLOR_MESSAGE,
          title: const Text(
            LOGIN_PROFILE_APPBAR,
            style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.normal),
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.arrow_forward_ios),
              onPressed: () {
                Navigator.pushReplacementNamed(context, LoginScreen.routName);
              },
              color: Colors.white,
            )
          ],
          leading: SizedBox.shrink(),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 20.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  LOGIN_PROFILE_TEXT,
                  style: profileStyle(context),
                ),
                SizedBox(
                  height: 20.0,
                ),
                TextFieldWidget(
                  textDirection: TextDirection.rtl,
                  text: 'نام :',
                  length: 50,
                  hintText: '',
                  obSecure: false,
                  textInputType: TextInputType.text,
                  textInputFormatter:
                      FilteringTextInputFormatter.singleLineFormatter,
                  textEditingController: controllerName,
                  textFunction: null,
                  isEnabled: true,
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextFieldWidget(
                  textDirection: TextDirection.rtl,
                  text: 'نام خانوادگی :',
                  length: 50,
                  hintText: '',
                  obSecure: false,
                  textInputType: TextInputType.text,
                  textInputFormatter:
                      FilteringTextInputFormatter.singleLineFormatter,
                  textEditingController: controllerFamily,
                  textFunction: null,
                  isEnabled: true,
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  child: TextDatePicker(
                      persianDatePicker: persianDatePicker,
                      textEditingController: textEditingController),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'جنسیت :',
                      style: profileStyle(context),
                    ),
                  ],
                ),
                Container(
                  child: Row(
                    children: [
                      Expanded(
                        child: RadioListTile<SingingCharacter>(
                          title: Text(
                            'مرد',
                            style: profileStyle(context),
                          ),
                          value: SingingCharacter.man,
                          groupValue: _gender,
                          onChanged: (SingingCharacter value) {
                            setState(() {
                              _gender = value;
                            });
                          },
                        ),
                      ),
                      Expanded(
                        child: RadioListTile<SingingCharacter>(
                          title: Text(
                            'زن',
                            style: profileStyle(context),
                          ),
                          value: SingingCharacter.woman,
                          groupValue: _gender,
                          onChanged: (SingingCharacter value) {
                            setState(() {
                              _gender = value;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  child: RaisedButton(
                    color: COLOR_MESSAGE,
                    onPressed: () {
                      profile();
                      print(token);
                    },
                    child: Text(
                      'ذخیره',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.normal),
                    ),
                  ),
                ),
                Container(
                  height: 30.0,
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: FittedBox(
                    child: Text(
                      companyName,
                      style: TextStyle(fontSize: 9.0),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void profile() {
    if (controllerName.text.isEmpty ||
        controllerFamily.text.isEmpty ||
        textEditingController.text.isEmpty) {
      Alert(
          closeFunction: () {},
          context: context,
          image: Image.asset(
            'assets/images/danger.png',
            width: 50.0,
            height: 50.0,
            fit: BoxFit.contain,
            color: Colors.red,
          ),
          style: AlertStyle(
            isOverlayTapDismiss: false,
            isCloseButton: false,
            titleStyle:
                TextStyle(fontSize: 13.0, fontWeight: FontWeight.normal),
          ),
          title: 'لطفا اطلاعات را تکمیل نمائید',
          buttons: [
            DialogButton(
              color: COLOR_MESSAGE,
              onPressed: () => Navigator.pop(context),
              child: Text(
                "بستن",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
            ),
          ]).show();
      print('لطفآ اطلاعات را تکمیل نمایید');
    } else {
      userProfile['name'] = controllerName.text;
      userProfile['family'] = controllerFamily.text;
      userProfile['birthday'] = textEditingController.text.trim();
      userProfile['gender'] = _gender.index == 0 ? true : false;
      print(userProfile);
      Alert(
          closeFunction: () {},
          context: context,
          image: Image.asset(
            'assets/images/danger.png',
            width: 50.0,
            height: 50.0,
            fit: BoxFit.contain,
            color: Colors.red,
          ),
          style: AlertStyle(
            isOverlayTapDismiss: false,
            isCloseButton: false,
            titleStyle:
                TextStyle(fontSize: 13.0, fontWeight: FontWeight.normal),
          ),
          title: 'آیا از صحت اطلاعات وارد شده اطمینان دارید ؟',
          content: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: AcceptDialog(
                      text: 'نام : ',
                      controller: controllerName.text,
                      context: context),
                ),
                AcceptDialog(
                    text: 'نام خانوادگی : ',
                    controller: controllerFamily.text,
                    context: context),
                AcceptDialog(
                    text: 'تاریخ تولد : ',
                    controller: textEditingController.text,
                    context: context),
                AcceptDialog(
                    text: 'جنسیت : ',
                    controller: _gender.index == 0 ? 'مرد' : 'زن',
                    context: context),
              ],
            ),
          ),
          buttons: [
            DialogButton(
              color: COLOR_MESSAGE,
              onPressed: () => Navigator.pop(context),
              child: Text(
                "خیر",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
            ),
            DialogButton(
              color: COLOR_MESSAGE,
              onPressed: () => setBaseProfile(context),
              child: Text(
                "بله",
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
            ),
          ]).show();
      print('آیا از صحت اطلاعات وارد شده اطمینان دارید ؟');
    }
  }

  void setBaseProfile(context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            padding: const EdgeInsets.all(10.0),
            width: MediaQuery.of(context).size.width * .6,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "لطفا چند لحظه صبر کنید ...".trim(),
                  style: TextStyle(fontWeight: FontWeight.normal),
                ),
                CircularProgressIndicator(),
              ],
            ),
          ),
        );
      },
    );
    Future.delayed(new Duration(milliseconds: 1), () async {
      if (await checkConnectivity()) {
        callProfile(
                '74-D4-35-5C-CD-F4',
                'IPhone6',
                '1.0.02',
                'xx',
                '1.0.02',
                '1000*200',
                '192.168.1.2',
                token,
                'SetBaseProfile',
                userProfile['name'],
                userProfile['family'],
                userProfile['birthday'],
                userProfile['gender'])
            .then((response) {
          if (response.statusCode == 200) {
            final jsonResponse = jsonDecode(response.data);
            print(jsonResponse);
            Map<String, dynamic> map = jsonResponse;
            if (map['Result']['ReturnValue'] == 3000 ||
                map['Result']['ReturnValue'] == 4000) {
              TokenExpird tokenExpird = TokenExpird.fromJson(jsonResponse);
              Alert(
                  closeFunction: () {},
                  context: context,
                  image: Image.asset(
                    'assets/images/danger.png',
                    width: 50.0,
                    height: 50.0,
                    fit: BoxFit.contain,
                    color: Colors.red,
                  ),
                  style: AlertStyle(
                    isOverlayTapDismiss: false,
                    isCloseButton: false,
                    titleStyle: TextStyle(
                        fontSize: 14.0, fontWeight: FontWeight.normal),
                  ),
                  title: tokenExpird.result.returnValueMessage,
                  buttons: [
                    DialogButton(
                      color: COLOR_MESSAGE,
                      onPressed: () => Navigator.pushNamed(
                          context, LoginProfileScreen.routName),
                      child: Text(
                        "بستن",
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    )
                  ]).show();
            } else if (map['Result']['ReturnValue'] == 0) {
              ProfileComplete profileComplete =
                  ProfileComplete.fromJson(jsonResponse);
              Navigator.pop(context);
              Alert(
                  closeFunction: () {},
                  context: context,
                  image: Image.asset(
                    'assets/images/danger.png',
                    width: 50.0,
                    height: 50.0,
                    fit: BoxFit.contain,
                    color: Colors.red,
                  ),
                  style: AlertStyle(
                    isOverlayTapDismiss: false,
                    isCloseButton: false,
                    titleStyle: TextStyle(
                        fontSize: 14.0, fontWeight: FontWeight.normal),
                  ),
                  title: profileComplete.result.returnValueMessage,
                  buttons: [
                    DialogButton(
                      color: COLOR_MESSAGE,
                      onPressed: () => Navigator.pushNamed(
                          context, LoginProfileScreen.routName),
                      child: Text(
                        "بستن",
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    )
                  ]).show();
              Navigator.pushReplacementNamed(context, DashboardScreen.routName);
            }
          }
        }).catchError((error) {
          Navigator.pushReplacementNamed(context, LoginScreen.routName);
          print(error);
        }).whenComplete(() {});
      } else if (await checkConnectivity() == false) {
        Navigator.pop(context);
        isConnect(context);
      }
    });
  }

  void isConnect(context) {
    Alert(
        closeFunction: () {},
        context: context,
        image: Image.asset(
          'assets/images/danger.png',
          width: 50.0,
          height: 50.0,
          fit: BoxFit.contain,
          color: Colors.red,
        ),
        style: AlertStyle(
          isOverlayTapDismiss: false,
          isCloseButton: false,
          titleStyle: TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
        ),
        title:
            'اتصال به اینترنت برقرار نمی باشد. لطفا اتصال اینترنت گوشی خود را بررسی نمایید.',
        buttons: [
          DialogButton(
            color: COLOR_MESSAGE,
            onPressed: () {
              connect();
              if (isConnected) {
                Navigator.pop(context);
              }
            },
            child: SizedBox(
              width: MediaQuery.of(context).size.width * .2,
              child: FittedBox(
                child: Text(
                  "تلاش مجدد",
                  style: TextStyle(color: Colors.white, fontSize: 13),
                ),
              ),
            ),
          )
        ]).show();
  }

  void connect() async {
    if (await checkConnectivity()) {
      setState(() {
        isConnected = true;
      });
    } else {
      setState(() {
        isConnected = false;
      });
    }
  }
}

class AcceptDialog extends StatelessWidget {
  const AcceptDialog({
    @required this.text,
    @required this.context,
    @required this.controller,
  });

  final String text;
  final String controller;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(text: text, style: acceptStyle(context)),
          TextSpan(text: controller, style: acceptStyle2(context)),
        ],
      ),
    );
  }
}

class TextDatePicker extends StatelessWidget {
  const TextDatePicker({
    Key key,
    @required this.persianDatePicker,
    @required this.textEditingController,
  }) : super(key: key);

  final PersianDatePickerWidget persianDatePicker;
  final TextEditingController textEditingController;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'تاریخ تولد :',
            style: profileStyle(context),
          ),
          TextField(
            style: profileStyle(context),
            enableInteractiveSelection: false,
            // *** this is important to prevent user interactive selection ***
            onTap: () {
              FocusScope.of(context).requestFocus(
                  FocusNode()); // to prevent opening default keyboard
              showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return persianDatePicker;
                  });
            },
            controller: textEditingController,
            decoration: InputDecoration(
              // errorStyle: TextStyle(
              //   fontSize: 11.0,
              // ),
              counterText: '',
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
              hintText: '',
              border: OutlineInputBorder(
                borderSide: BorderSide(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
