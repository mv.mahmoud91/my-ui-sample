import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const INTERNET_OFF_MESSAGE =
    'اتصال به اینترنت برقرار نمی باشد. لطفا اتصال اینترنت گوشی خود را بررسی نمائید.';
const Color COLOR_MESSAGE = Color(0xFF1D8913);

TextStyle loginStyle(BuildContext context) {
  return Theme.of(context).textTheme.headline6.copyWith(
      color: Colors.black, fontSize: 11.0, fontWeight: FontWeight.normal);
}

const String USER_RULES_MESSAGE1 =
    'پایگاه اینترنتی اتوماسیون یکپارچه کانون اعتباری ما "ایکام" و اعضاء این باشگاه، مقید به رعایت قوانین تجارت الکترونیکی و جرایم رایانه ای و سایر قوانین مربوطه می باشند.ضمنآ مقررات ذیل مورد قبول تمامی اعضاء بوده واین باشگاه در صورت لزوم میتواند در شرایط ومقررات آن تغییرات لازم اعمال نماید. بر این اساس تنها مرجع اطلاع از قوانین و مقررات به روز باشگاه، سایت ایکام بوده وتمامی اعضاء ملزم به مطالعه و پیگیری اخبار و اطلاعیه های صادره می باشند. خدمات و محتویات این پایگاه برای استفاده اعضاء باشگاه مشتریان ایکام عرضه شده است و کلیه عناصر موجود دراین وب سایت، شامل اصلاعات، اسناد، تولیدات، لوگوها، گرافیک، تصاویر و خدمات، کلا متعلق به سایت ایکام می باشد و هیچ شخص حقیقی و حقوقی بدون اجازه کتبی صاحب عناصر مذکور سایت ایکام اجازه کپی، توزیع، نشر مجدد، واگذاری، نمایش، ارسال و انتقال آن ها راندارد. معرفی هریک از سایت های لینک شده دلیل بر تایید آن سایت نمی باشد و مسئولیت هر گونه استفاده از وب سایت با استناد بر سایت های لینک شده آن بر عهده استفاده کننده می باشد.';
const String USER_RULES_MESSAGE2 =
    'شرایط و ضوابط عضویت در باشگاه مشتریان ایکام';
const List<String> USER_RULES_MESSAGE3 = [
  '*تنها اشخاص حقیقی حق عضویت در باشگاه را دارند.',
  '* عضویت در باشگاه به منزله قبول و پذیرش کلیه قوانین و مقررات آن می باشد',
];

const String INTERNET_AGAIN_TRY = 'تلاش مجدد';
const String USER_NAME_TEXT = 'نام کاربری :';
const String USER_PASSWORD_TEXT = 'کلمه عبور :';
const String USER_CODE_TEXT = 'کد ملی :';
const String LOGIN_PROFILE_APPBAR = 'تکمیل پروفایل';
const String LOGIN_PROFILE_TEXT =
    'جهت استفاده از سامانه الزامیست اطلاعات خواسته شده را به درستی وارد نمایید:';
const String USER_PHONE_NUMBER_TEXT = 'شماره همراه :';
const String USER_RULES_TEXT = 'قوانین مورد تایید می باشد.';
const String USER_SHOW_RULES_TEXT = 'مشاهده قوانین';
const String USER_SAVE_PASSWORD_TEXT = 'ذخیره کلمه عبور';
const String USER_RECOVERY_PASSWORD_TEXT = 'بازیابی کلمه عبور';
const String USER_RECOVERY_PASSWORD_MESSAGE =
    'جهت بازیابی کلمه عبور، نام کاربری خودرا وارد نمایید:';
const String USER_JOIN_CLUB_TEXT = 'عضویت در باشگاه';
const String USER_SIGN_UP_TEXT = 'عضویت';
const String USER_ENTER_TEXT = 'ورود';
const String USER_RECOVERY_AGAIN_TEXT = 'بازیابی مجدد کلمه عبور';
const String USER_SIGN_UP_MESSAGE =
    'درصورتی که قبلا ثبت نام نکرده اید، در باشگاه عضو شوید';
const String USER_SIGN_IN_MESSAGE =
    'درصورتی که قبلا ثبت نام کرده اید، وارد باشگاه شوید';
const String CHARGE_WALLET_MUST =
    'مبلغ باید بین 10000 تا 20000000000 ریال باشد.';
const String FINGER_PRINT_TEXT =
    'ورود با اثر انگشت برای شما فعال نمی باشد. لطفا پس از ورود با نام کاربری و کلمه عبور خود، وارد بخش تنظیمات شده و گزینه قابلیت ورود با اثر انگشت را فعال نمایید.';

TextStyle chargeStyle(BuildContext context) {
  return Theme.of(context).textTheme.headline5.copyWith(
      color: Colors.black, fontSize: 11.0, fontWeight: FontWeight.normal);
}

TextStyle profileStyle(BuildContext context) {
  return Theme.of(context).textTheme.headline5.copyWith(
      color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.normal);
}

TextStyle qrStyle(BuildContext context) {
  return Theme.of(context).textTheme.headline5.copyWith(
      color: Colors.black, fontSize: 14.0, fontWeight: FontWeight.normal);
}

TextStyle acceptStyle2(BuildContext context) {
  return Theme.of(context).textTheme.headline5.copyWith(
      color: Color(0xFF1D8913), fontSize: 12.0, fontWeight: FontWeight.normal);
}

TextStyle acceptStyle(BuildContext context) {
  return Theme.of(context).textTheme.headline5.copyWith(
      color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.normal);
}
