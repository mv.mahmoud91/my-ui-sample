import 'dart:convert';

class LoadingApp {
  LoadingApp({
    this.result,
    this.initialize,
  });

  final Result result;
  final List<Initialize> initialize;

  factory LoadingApp.fromRawJson(String str) =>
      LoadingApp.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory LoadingApp.fromJson(Map<String, dynamic> json) => LoadingApp(
        result: Result.fromJson(json["Result"]),
        initialize: List<Initialize>.from(
            json["Initialize"].map((x) => Initialize.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
        "Initialize": List<dynamic>.from(initialize.map((x) => x.toJson())),
      };
}

class Initialize {
  Initialize({
    this.shopeId,
    this.logo,
    this.logoUrl,
    this.brandName,
    this.brandColor,
    this.foreColor,
    this.companyName,
  });

  final int shopeId;
  final String logo;
  final String logoUrl;
  final String brandName;
  final String brandColor;
  final String foreColor;
  final String companyName;

  factory Initialize.fromRawJson(String str) =>
      Initialize.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Initialize.fromJson(Map<String, dynamic> json) => Initialize(
        shopeId: json["ShopeID"],
        logo: json["Logo"],
        logoUrl: json["LogoURL"],
        brandName: json["BrandName"],
        brandColor: json["BrandColor"],
        foreColor: json["ForeColor"],
        companyName: json["CompanyName"],
      );

  Map<String, dynamic> toJson() => {
        "ShopeID": shopeId,
        "Logo": logo,
        "LogoURL": logoUrl,
        "BrandName": brandName,
        "BrandColor": brandColor,
        "ForeColor": foreColor,
        "CompanyName": companyName,
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromRawJson(String str) => Result.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}
