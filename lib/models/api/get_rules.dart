import 'dart:convert';

GetRules getRulesFromJson(String str) => GetRules.fromJson(json.decode(str));

String getRulesToJson(GetRules data) => json.encode(data.toJson());

class GetRules {
  GetRules({
    this.result,
    this.rules,
  });

  final Result result;
  final Rules rules;

  factory GetRules.fromJson(Map<String, dynamic> json) => GetRules(
        result: Result.fromJson(json["Result"]),
        rules: Rules.fromJson(json["Rules"]),
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
        "Rules": rules.toJson(),
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}

class Rules {
  Rules({
    this.context,
  });

  final String context;

  factory Rules.fromJson(Map<String, dynamic> json) => Rules(
        context: json["Context"],
      );

  Map<String, dynamic> toJson() => {
        "Context": context,
      };
}
