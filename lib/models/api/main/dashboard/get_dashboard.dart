import 'dart:convert';

class GetDashboard {
  GetDashboard({
    this.result,
    this.account,
    this.memberInfo,
    this.notification,
    this.slider,
    this.token,
  });

  final Result result;
  final Account account;
  final MemberInfo memberInfo;
  final Notification notification;
  final List<Slider> slider;
  final dynamic token;

  factory GetDashboard.fromRawJson(String str) =>
      GetDashboard.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetDashboard.fromJson(Map<String, dynamic> json) => GetDashboard(
        result: Result.fromJson(json["Result"]),
        account: Account.fromJson(json["Account"]),
        memberInfo: MemberInfo.fromJson(json["MemberInfo"]),
        notification: Notification.fromJson(json["Notification"]),
        slider:
            List<Slider>.from(json["Slider"].map((x) => Slider.fromJson(x))),
        token: json["Token"],
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
        "Account": account.toJson(),
        "MemberInfo": memberInfo.toJson(),
        "Notification": notification.toJson(),
        "Slider": List<dynamic>.from(slider.map((x) => x.toJson())),
        "Token": token,
      };
}

class Account {
  Account({
    this.customerId,
    this.refId,
    this.sumCredit,
    this.sumDebit,
    this.remain,
    this.level,
    this.levelName,
    this.unitType,
  });

  final int customerId;
  final String refId;
  final int sumCredit;
  final int sumDebit;
  final int remain;
  final int level;
  final String levelName;
  final String unitType;

  factory Account.fromRawJson(String str) => Account.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Account.fromJson(Map<String, dynamic> json) => Account(
        customerId: json["CustomerID"],
        refId: json["RefID"],
        sumCredit: json["SumCredit"],
        sumDebit: json["SumDebit"],
        remain: json["Remain"],
        level: json["level"],
        levelName: json["levelName"],
        unitType: json["UnitType"],
      );

  Map<String, dynamic> toJson() => {
        "CustomerID": customerId,
        "RefID": refId,
        "SumCredit": sumCredit,
        "SumDebit": sumDebit,
        "Remain": remain,
        "level": level,
        "levelName": levelName,
        "UnitType": unitType,
      };
}

class MemberInfo {
  MemberInfo({
    this.customerId,
    this.firstName,
    this.lastName,
    this.photo,
    this.barcodeImage,
    this.barcodeImageString,
    this.gender,
    this.genderName,
    this.brithDate,
  });

  final int customerId;
  final String firstName;
  final String lastName;
  final String photo;
  final String barcodeImage;
  final dynamic barcodeImageString;
  final bool gender;
  final String genderName;
  final dynamic brithDate;

  factory MemberInfo.fromRawJson(String str) =>
      MemberInfo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MemberInfo.fromJson(Map<String, dynamic> json) => MemberInfo(
        customerId: json["CustomerID"],
        firstName: json["FirstName"],
        lastName: json["LastName"],
        photo: json["Photo"],
        barcodeImage: json["BarcodeImage"],
        barcodeImageString: json["BarcodeImageString"],
        gender: json["Gender"],
        genderName: json["GenderName"],
        brithDate: json["BrithDate"],
      );

  Map<String, dynamic> toJson() => {
        "CustomerID": customerId,
        "FirstName": firstName,
        "LastName": lastName,
        "Photo": photo,
        "BarcodeImage": barcodeImage,
        "BarcodeImageString": barcodeImageString,
        "Gender": gender,
        "GenderName": genderName,
        "BrithDate": brithDate,
      };
}

class Notification {
  Notification({
    this.numberNotification,
  });

  final int numberNotification;

  factory Notification.fromRawJson(String str) =>
      Notification.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Notification.fromJson(Map<String, dynamic> json) => Notification(
        numberNotification: json["NumberNotification"],
      );

  Map<String, dynamic> toJson() => {
        "NumberNotification": numberNotification,
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromRawJson(String str) => Result.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}

class Slider {
  Slider({
    this.id,
    this.title,
    this.photoUrl,
  });

  final int id;
  final String title;
  final String photoUrl;

  factory Slider.fromRawJson(String str) => Slider.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Slider.fromJson(Map<String, dynamic> json) => Slider(
        id: json["ID"],
        title: json["Title"],
        photoUrl: json["PhotoURL"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
        "Title": title,
        "PhotoURL": photoUrl,
      };
}
