import 'package:flutter/material.dart';

class Dashboard {
  final String iconUrl;
  final String title;
  Dashboard({@required this.iconUrl, @required this.title});
}
