import 'dart:convert';

class GetPaymentGateWayInfo {
  GetPaymentGateWayInfo({
    this.result,
    this.paymentTerminal,
    this.paymentGateway,
    this.description,
  });

  final Result result;
  final List<PaymentTerminal> paymentTerminal;
  final PaymentGateway paymentGateway;
  final String description;

  factory GetPaymentGateWayInfo.fromRawJson(String str) =>
      GetPaymentGateWayInfo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory GetPaymentGateWayInfo.fromJson(Map<String, dynamic> json) =>
      GetPaymentGateWayInfo(
        result: Result.fromJson(json["Result"]),
        paymentTerminal: List<PaymentTerminal>.from(
            json["PaymentTerminal"].map((x) => PaymentTerminal.fromJson(x))),
        paymentGateway: PaymentGateway.fromJson(json["PaymentGateway"]),
        description: json["Description"],
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
        "PaymentTerminal":
            List<dynamic>.from(paymentTerminal.map((x) => x.toJson())),
        "PaymentGateway": paymentGateway.toJson(),
        "Description": description,
      };
}

class PaymentGateway {
  PaymentGateway({
    this.description,
    this.walletBalance,
  });

  final String description;
  final int walletBalance;

  factory PaymentGateway.fromRawJson(String str) =>
      PaymentGateway.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentGateway.fromJson(Map<String, dynamic> json) => PaymentGateway(
        description: json["Description"],
        walletBalance: json["WalletBalance"],
      );

  Map<String, dynamic> toJson() => {
        "Description": description,
        "WalletBalance": walletBalance,
      };
}

class PaymentTerminal {
  PaymentTerminal({
    this.terminalId,
    this.terminalName,
    this.terminalLogo,
    this.maxCharge,
    this.minCharge,
  });

  final int terminalId;
  final String terminalName;
  final String terminalLogo;
  final int maxCharge;
  final int minCharge;

  factory PaymentTerminal.fromRawJson(String str) =>
      PaymentTerminal.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentTerminal.fromJson(Map<String, dynamic> json) =>
      PaymentTerminal(
        terminalId: json["TerminalID"],
        terminalName: json["TerminalName"],
        terminalLogo: json["TerminalLogo"],
        maxCharge: json["MaxCharge"],
        minCharge: json["MinCharge"],
      );

  Map<String, dynamic> toJson() => {
        "TerminalID": terminalId,
        "TerminalName": terminalName,
        "TerminalLogo": terminalLogo,
        "MaxCharge": maxCharge,
        "MinCharge": minCharge,
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromRawJson(String str) => Result.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}
