import 'dart:convert';

class ChargeUrl {
  ChargeUrl({
    this.result,
    this.paymentTransactionToken,
  });

  final Result result;
  final PaymentTransactionToken paymentTransactionToken;

  factory ChargeUrl.fromRawJson(String str) =>
      ChargeUrl.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ChargeUrl.fromJson(Map<String, dynamic> json) => ChargeUrl(
        result: Result.fromJson(json["Result"]),
        paymentTransactionToken:
            PaymentTransactionToken.fromJson(json["PaymentTransactionToken"]),
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
        "PaymentTransactionToken": paymentTransactionToken.toJson(),
      };
}

class PaymentTransactionToken {
  PaymentTransactionToken({
    this.orderId,
    this.sentBankScript,
    this.bankTokenId,
    this.urlRouter,
    this.bankId,
  });

  final int orderId;
  final String sentBankScript;
  final String bankTokenId;
  final String urlRouter;
  final int bankId;

  factory PaymentTransactionToken.fromRawJson(String str) =>
      PaymentTransactionToken.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentTransactionToken.fromJson(Map<String, dynamic> json) =>
      PaymentTransactionToken(
        orderId: json["OrderID"],
        sentBankScript: json["SentBankScript"],
        bankTokenId: json["BankTokenID"],
        urlRouter: json["URLRouter"],
        bankId: json["BankID"],
      );

  Map<String, dynamic> toJson() => {
        "OrderID": orderId,
        "SentBankScript": sentBankScript,
        "BankTokenID": bankTokenId,
        "URLRouter": urlRouter,
        "BankID": bankId,
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromRawJson(String str) => Result.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}
