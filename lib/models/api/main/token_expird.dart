import 'dart:convert';

class TokenExpird {
  TokenExpird({
    this.result,
  });

  final Result result;

  factory TokenExpird.fromRawJson(String str) =>
      TokenExpird.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory TokenExpird.fromJson(Map<String, dynamic> json) => TokenExpird(
        result: Result.fromJson(json["Result"]),
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromRawJson(String str) => Result.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}
