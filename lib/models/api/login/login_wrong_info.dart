import 'dart:convert';

class LoginWrongInfo {
  LoginWrongInfo({
    this.result,
  });

  final Result result;

  factory LoginWrongInfo.fromRawJson(String str) =>
      LoginWrongInfo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory LoginWrongInfo.fromJson(Map<String, dynamic> json) => LoginWrongInfo(
        result: Result.fromJson(json["Result"]),
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromRawJson(String str) => Result.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}
