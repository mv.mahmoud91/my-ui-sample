import 'dart:convert';

class ProfileNotComplete {
  ProfileNotComplete({
    this.result,
    this.shopes,
    this.token,
    this.userName,
  });

  final Result result;
  final List<Shope> shopes;
  final String token;
  final String userName;

  factory ProfileNotComplete.fromRawJson(String str) =>
      ProfileNotComplete.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProfileNotComplete.fromJson(Map<String, dynamic> json) =>
      ProfileNotComplete(
        result: Result.fromJson(json["Result"]),
        shopes: List<Shope>.from(json["Shopes"].map((x) => Shope.fromJson(x))),
        token: json["Token"],
        userName: json["UserName"],
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
        "Shopes": List<dynamic>.from(shopes.map((x) => x.toJson())),
        "Token": token,
        "UserName": userName,
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromRawJson(String str) => Result.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}

class Shope {
  Shope({
    this.shopeId,
    this.brandAgent,
    this.brandName,
    this.qRcode,
  });

  final int shopeId;
  final dynamic brandAgent;
  final dynamic brandName;
  final dynamic qRcode;

  factory Shope.fromRawJson(String str) => Shope.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Shope.fromJson(Map<String, dynamic> json) => Shope(
        shopeId: json["ShopeID"],
        brandAgent: json["BrandAgent"],
        brandName: json["BrandName"],
        qRcode: json["QRcode"],
      );

  Map<String, dynamic> toJson() => {
        "ShopeID": shopeId,
        "BrandAgent": brandAgent,
        "BrandName": brandName,
        "QRcode": qRcode,
      };
}
