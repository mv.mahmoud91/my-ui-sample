import 'dart:convert';

class SignUpOk {
  SignUpOk({
    this.result,
    this.member,
  });

  final Result result;
  final Member member;

  factory SignUpOk.fromRawJson(String str) =>
      SignUpOk.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory SignUpOk.fromJson(Map<String, dynamic> json) => SignUpOk(
        result: Result.fromJson(json["Result"]),
        member: Member.fromJson(json["Member"]),
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
        "Member": member.toJson(),
      };
}

class Member {
  Member({
    this.mobile,
  });

  final String mobile;

  factory Member.fromRawJson(String str) => Member.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Member.fromJson(Map<String, dynamic> json) => Member(
        mobile: json["Mobile"],
      );

  Map<String, dynamic> toJson() => {
        "Mobile": mobile,
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromRawJson(String str) => Result.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}
