import 'dart:convert';

class RecoveryError {
  RecoveryError({
    this.result,
  });

  final Result result;

  factory RecoveryError.fromRawJson(String str) =>
      RecoveryError.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RecoveryError.fromJson(Map<String, dynamic> json) => RecoveryError(
        result: Result.fromJson(json["Result"]),
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromRawJson(String str) => Result.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}
