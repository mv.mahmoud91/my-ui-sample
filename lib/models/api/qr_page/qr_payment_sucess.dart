// To parse this JSON data, do
//
//     final qrPaymentSuccess = qrPaymentSuccessFromJson(jsonString);

import 'dart:convert';

QrPaymentSuccess qrPaymentSuccessFromJson(String str) =>
    QrPaymentSuccess.fromJson(json.decode(str));

String qrPaymentSuccessToJson(QrPaymentSuccess data) =>
    json.encode(data.toJson());

class QrPaymentSuccess {
  QrPaymentSuccess({
    this.result,
    this.payment,
    this.paymentTransactionToken,
  });

  final Result result;
  final List<dynamic> payment;
  final PaymentTransactionToken paymentTransactionToken;

  factory QrPaymentSuccess.fromJson(Map<String, dynamic> json) =>
      QrPaymentSuccess(
        result: Result.fromJson(json["Result"]),
        payment: List<dynamic>.from(json["Payment"].map((x) => x)),
        paymentTransactionToken:
            PaymentTransactionToken.fromJson(json["PaymentTransactionToken"]),
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
        "Payment": List<dynamic>.from(payment.map((x) => x)),
        "PaymentTransactionToken": paymentTransactionToken.toJson(),
      };
}

class PaymentTransactionToken {
  PaymentTransactionToken({
    this.orderId,
    this.sentBankScript,
    this.bankTokenId,
    this.urlRouter,
    this.bankId,
  });

  final int orderId;
  final String sentBankScript;
  final String bankTokenId;
  final String urlRouter;
  final int bankId;

  factory PaymentTransactionToken.fromJson(Map<String, dynamic> json) =>
      PaymentTransactionToken(
        orderId: json["OrderID"],
        sentBankScript: json["SentBankScript"],
        bankTokenId: json["BankTokenID"],
        urlRouter: json["URLRouter"],
        bankId: json["BankID"],
      );

  Map<String, dynamic> toJson() => {
        "OrderID": orderId,
        "SentBankScript": sentBankScript,
        "BankTokenID": bankTokenId,
        "URLRouter": urlRouter,
        "BankID": bankId,
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}
