import 'dart:convert';

GetBankCard getBankCardFromJson(String str) =>
    GetBankCard.fromJson(json.decode(str));

String getBankCardToJson(GetBankCard data) => json.encode(data.toJson());

class GetBankCard {
  GetBankCard({
    this.result,
    this.bankCard,
  });

  final Result result;
  final List<BankCard> bankCard;

  factory GetBankCard.fromJson(Map<String, dynamic> json) => GetBankCard(
        result: Result.fromJson(json["Result"]),
        bankCard: List<BankCard>.from(
            json["BankCard"].map((x) => BankCard.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
        "BankCard": List<dynamic>.from(bankCard.map((x) => x.toJson())),
      };
}

class BankCard {
  BankCard({
    this.cardNo,
    this.bankName,
    this.bankLogo,
    this.isVerification,
    this.verificationText,
    this.verificationColor,
  });

  final String cardNo;
  final String bankName;
  final String bankLogo;
  final bool isVerification;
  final String verificationText;
  final String verificationColor;

  factory BankCard.fromJson(Map<String, dynamic> json) => BankCard(
        cardNo: json["CardNo"],
        bankName: json["BankName"],
        bankLogo: json["BankLogo"],
        isVerification: json["IsVerification"],
        verificationText: json["VerificationText"],
        verificationColor: json["VerificationColor"],
      );

  Map<String, dynamic> toJson() => {
        "CardNo": cardNo,
        "BankName": bankName,
        "BankLogo": bankLogo,
        "IsVerification": isVerification,
        "VerificationText": verificationText,
        "VerificationColor": verificationColor,
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}
