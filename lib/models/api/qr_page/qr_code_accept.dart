// To parse this JSON data, do
//
//     final qrCodeAccept = qrCodeAcceptFromJson(jsonString);

import 'dart:convert';

QrCodeAccept qrCodeAcceptFromJson(String str) =>
    QrCodeAccept.fromJson(json.decode(str));

String qrCodeAcceptToJson(QrCodeAccept data) => json.encode(data.toJson());

class QrCodeAccept {
  QrCodeAccept({
    this.result,
    this.payment,
  });

  final Result result;
  final Payment payment;

  factory QrCodeAccept.fromJson(Map<String, dynamic> json) => QrCodeAccept(
        result: Result.fromJson(json["Result"]),
        payment: Payment.fromJson(json["Payment"]),
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
        "Payment": payment.toJson(),
      };
}

class Payment {
  Payment({
    this.brandAgent,
    this.brandName,
    this.point,
    this.brandPhoto,
    this.qRcodeOut,
  });

  final String brandAgent;
  final String brandName;
  final int point;
  final String brandPhoto;
  final String qRcodeOut;

  factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        brandAgent: json["BrandAgent"],
        brandName: json["BrandName"],
        point: json["Point"],
        brandPhoto: json["BrandPhoto"],
        qRcodeOut: json["QRcodeOut"],
      );

  Map<String, dynamic> toJson() => {
        "BrandAgent": brandAgent,
        "BrandName": brandName,
        "Point": point,
        "BrandPhoto": brandPhoto,
        "QRcodeOut": qRcodeOut,
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}
