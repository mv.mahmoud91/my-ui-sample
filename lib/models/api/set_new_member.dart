import 'dart:convert';

class SetNewMember {
  SetNewMember({
    this.result,
  });

  final Result result;

  factory SetNewMember.fromRawJson(String str) =>
      SetNewMember.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory SetNewMember.fromJson(Map<String, dynamic> json) => SetNewMember(
        result: Result.fromJson(json["Result"]),
      );

  Map<String, dynamic> toJson() => {
        "Result": result.toJson(),
      };
}

class Result {
  Result({
    this.returnValue,
    this.returnValueMessage,
  });

  final int returnValue;
  final String returnValueMessage;

  factory Result.fromRawJson(String str) => Result.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        returnValue: json["ReturnValue"],
        returnValueMessage: json["ReturnValueMessage"],
      );

  Map<String, dynamic> toJson() => {
        "ReturnValue": returnValue,
        "ReturnValueMessage": returnValueMessage,
      };
}
