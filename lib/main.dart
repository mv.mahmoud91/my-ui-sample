import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import './provider/dashboard_screen/qr_wallet.dart';
import 'screens/dashboard/pay_screen.dart';
import './provider/dashboard_screen/dashboard_screen_provider.dart';
import './provider/login_screen/provider_login.dart';
import 'screens/login/login_profile_screen.dart';
import 'screens/dashboard/charge_wallet_screen.dart';
import 'screens/dashboard/qr_wallet_screen.dart';
import 'screens/login/login_screen.dart';
import './screens/splash_screen.dart';
import 'screens/dashboard/dashboard_screen.dart';
import './provider/login_screen/provider_validate.dart';
import './provider/login_screen/provider_checkbox.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => QrWallet(),
        ),
        ChangeNotifierProvider(
          create: (_) => CheckBoxProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => ValidateProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => LoginProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => ProviderDashBoardScreen(),
        )
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'IranSans',
        inputDecorationTheme: InputDecorationTheme(
          border: InputBorder.none,
        ),
      ),
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        DefaultCupertinoLocalizations.delegate
      ],
      supportedLocales: [
//          Locale("en", "US"),
        Locale("fa", "IR"),
      ],
      initialRoute: SplashScreen.routName,
      routes: <String, WidgetBuilder>{
        SplashScreen.routName: (context) => SplashScreen(),
        LoginScreen.routName: (context) => LoginScreen(),
        LoginProfileScreen.routName: (context) => LoginProfileScreen(),
        ChargeWalletScreen.routName: (context) => ChargeWalletScreen(),
        DashboardScreen.routName: (context) => DashboardScreen(),
        QrWalletScreen.routName: (context) => QrWalletScreen(),
        PayScreen.routName: (context) => PayScreen(),
      },
    );
  }
}
