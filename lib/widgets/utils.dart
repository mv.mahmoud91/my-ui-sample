import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import '../screens/login/login_screen.dart';

// FarsiNumbers and MoneyFormat ===========================================================================================================================
extension StringExtension on String {
  String toLatin() {
    return this
        .replaceAll("۰", "0")
        .replaceAll("۱", "1")
        .replaceAll("۲", "2")
        .replaceAll("۳", "3")
        .replaceAll("۴", "4")
        .replaceAll("۵", "5")
        .replaceAll("۶", "6")
        .replaceAll("۷", "7")
        .replaceAll("۸", "8")
        .replaceAll("۹", "9")
        .replaceAll("٪", "%");
  }

  String toFarsi() {
    return this
        .replaceAll("0", "۰")
        .replaceAll("1", "۱")
        .replaceAll("2", "۲")
        .replaceAll("3", "۳")
        .replaceAll("4", "۴")
        .replaceAll("5", "۵")
        .replaceAll("6", "۶")
        .replaceAll("7", "۷")
        .replaceAll("8", "۸")
        .replaceAll("9", "۹")
        .replaceAll("%", "٪");
  }

  String toMoneyFormat() {
    return this.replaceAllMapped(RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'),
        (Match match) => '${match[1]},');
  }
}

// FarsiNumbers and MoneyFormat End ===========================================================================================================================
// InputFormatter ===========================================================================================================================
class CurrencyInputFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length == 0) {
      return newValue.copyWith(text: '');
    } else if (newValue.text.compareTo(oldValue.text) != 0) {
      int selectionIndexFromTheRight =
          newValue.text.length - newValue.selection.end;
      final f = new NumberFormat("#,###");
      int num = int.parse(newValue.text.replaceAll(f.symbols.GROUP_SEP, ''));
      final newString = f.format(num);
      return new TextEditingValue(
        text: newString,
        selection: TextSelection.collapsed(
            offset: newString.length - selectionIndexFromTheRight),
      );
    } else {
      return newValue;
    }
  }
}

// InputFormatter END ======================================================================================================================
// ValidNationalCode  ======================================================================================================================
bool validateNationalCode(String nc) {
  if (nc.trim() == '') {
    return false;
  } else if (nc.length != 10) {
    return false;
  } else {
    int sum = 0;

    for (int i = 0; i < 9; i++) {
      sum += int.parse(nc[i]) * (10 - i);
    }

    int lastDigit;
    int divideRemaining = sum % 11;

    if (divideRemaining < 2) {
      lastDigit = divideRemaining;
    } else {
      lastDigit = 11 - (divideRemaining);
    }

    if (int.parse(nc[9]) == lastDigit) {
      return true;
    } else {
      return false;
    }
  }
}

// ValidNationalCode End  ======================================================================================================================
// ValidAmountCharge  ======================================================================================================================
bool validateAmountCharge(String nc) {
  String newValue = nc;
  String s = newValue.replaceAll(RegExp(','), '');
  print(newValue);
  if (nc.trim() == '') {
    return false;
  } else if (s.length < 5 || int.parse(s) > 20000000000) {
    return false;
  } else {
    return true;
  }
}

// ValidAmountCharge End  ======================================================================================================================
// fingerAlert  ======================================================================================================================
void fingerAlert(fingerContext, String title, Color color) {
  Alert(
      closeFunction: () {},
      context: fingerContext,
      image: Image.asset(
        'assets/images/danger.png',
        width: 50.0,
        height: 50.0,
        fit: BoxFit.contain,
        color: Colors.red,
      ),
      style: AlertStyle(
        isOverlayTapDismiss: false,
        isCloseButton: false,
        titleStyle: TextStyle(fontSize: 12.0, fontWeight: FontWeight.normal),
      ),
      title: title,
      buttons: [
        DialogButton(
          color: color,
          onPressed: () => Navigator.pop(fingerContext),
          child: Text(
            "بستن",
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
        )
      ]).show();
}

// fingerAlert End  ======================================================================================================================
// alertMessageDialog  ======================================================================================================================
void alertMessage(context, String title, Color color) {
  Alert(
      closeFunction: () {},
      context: context,
      image: Image.asset(
        'assets/images/danger.png',
        width: 50.0,
        height: 50.0,
        fit: BoxFit.contain,
        color: Colors.red,
      ),
      style: AlertStyle(
        isOverlayTapDismiss: false,
        isCloseButton: false,
        titleStyle: TextStyle(fontSize: 14.0, fontWeight: FontWeight.normal),
      ),
      title: title,
      buttons: [
        DialogButton(
          color: color,
          onPressed: () => Navigator.pop(context),
          child: Text(
            "بستن",
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
        )
      ]).show();
}

// alertMessageDialog End  ======================================================================================================================
// showLoadingDialog  ======================================================================================================================
void showLoading(context) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Dialog(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width * .6,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "لطفا چند لحظه صبر کنید ...".trim(),
                style: TextStyle(fontWeight: FontWeight.normal),
              ),
              CircularProgressIndicator(),
            ],
          ),
        ),
      );
    },
  );
}

// showLoadingDialog End  ======================================================================================================================
void errorOrTokenExpirdAlert(context, String title, Color color) {
  Alert(
      closeFunction: () {},
      context: context,
      image: Image.asset(
        'assets/images/danger.png',
        width: 50.0,
        height: 50.0,
        fit: BoxFit.contain,
        color: Colors.red,
      ),
      style: AlertStyle(
        isOverlayTapDismiss: false,
        isCloseButton: false,
        titleStyle: TextStyle(fontSize: 14.0, fontWeight: FontWeight.normal),
      ),
      title: title,
      buttons: [
        DialogButton(
          color: color,
          onPressed: () {
            Navigator.pushNamedAndRemoveUntil(
                context, LoginScreen.routName, (_) => false);
          },
          child: Text(
            "بستن",
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
        )
      ]).show();
}

// connectionDialog       ======================================================================================================================
void connectionDialog(
    context, Color color, Function connect, bool isConnected) {
  Alert(
      closeFunction: () {},
      context: context,
      image: Image.asset(
        'assets/images/danger.png',
        width: 50.0,
        height: 50.0,
        fit: BoxFit.contain,
        color: Colors.red,
      ),
      style: AlertStyle(
        isOverlayTapDismiss: false,
        isCloseButton: false,
        titleStyle: TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
      ),
      title:
          'اتصال به اینترنت برقرار نمی باشد. لطفا اتصال اینترنت گوشی خود را بررسی نمایید.',
      buttons: [
        DialogButton(
          color: color,
          onPressed: () {
            connect();
            if (isConnected) {
              Navigator.pop(context);
            }
          },
          child: SizedBox(
            width: MediaQuery.of(context).size.width * .2,
            child: FittedBox(
              child: Text(
                "تلاش مجدد",
                style: TextStyle(color: Colors.white, fontSize: 13),
              ),
            ),
          ),
        )
      ]).show();
}
// connectionDialog End  ======================================================================================================================
// qrAlertMessage        ======================================================================================================================

void qrAlertMessage(
    context, String title, Color color, QRViewController controller) {
  Alert(
      closeFunction: () {},
      context: context,
      image: Image.asset(
        'assets/images/danger.png',
        width: 50.0,
        height: 50.0,
        fit: BoxFit.contain,
        color: Colors.red,
      ),
      style: AlertStyle(
        isOverlayTapDismiss: false,
        isCloseButton: false,
        titleStyle: TextStyle(fontSize: 14.0, fontWeight: FontWeight.normal),
      ),
      title: title,
      buttons: [
        DialogButton(
          color: color,
          onPressed: () {
            Navigator.pop(context);
            controller.resumeCamera();
          },
          child: Text(
            "بستن",
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
        )
      ]).show();
}
// qrAlertMessage End       ======================================================================================================================
