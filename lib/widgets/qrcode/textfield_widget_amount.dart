import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../provider/dashboard_screen/dashboard_screen_provider.dart';
import '../../constants/constants.dart';
import '../../widgets/utils.dart';

class TextFieldWidgetQrCode extends StatelessWidget {
  TextFieldWidgetQrCode({this.money});
  final String money;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45.0,
      padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        border: Border.all(color: Colors.grey[400]),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 14,
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: FittedBox(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Text(
                        'موجودی کیف پول :',
                        style: qrStyle(context),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: FittedBox(
                    child: Text(
                      money.toMoneyFormat() + ' ' + 'ریال ',
                      style: qrStyle(context),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: SizedBox(
              width: double.infinity,
              height: double.maxFinite,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                child: RaisedButton(
                  padding: EdgeInsets.all(0),
                  color: COLOR_MESSAGE,
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                    size: 30.0,
                  ),
                  onPressed: () {
                    Provider.of<ProviderDashBoardScreen>(context, listen: false)
                        .setBnbIndex(0);
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
