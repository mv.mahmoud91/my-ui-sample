import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import '../../constants/constants.dart';
import '../../provider/dashboard_screen/dashboard_screen_provider.dart';

class BNBWidget extends StatefulWidget {
  @override
  _BNBWidgetState createState() => _BNBWidgetState();
}

class _BNBWidgetState extends State<BNBWidget> {
  List<String> bnbIcon = [
    'assets/images/mojudi-kif.svg',
    'assets/images/mojudi.svg',
    'assets/images/top-logo.svg',
    'assets/images/surathesab.svg',
    'assets/images/pardakht-out-card.svg',
  ];

  @override
  Widget build(BuildContext context) {
    final provider =
        Provider.of<ProviderDashBoardScreen>(context, listen: false);
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25.0),
        ),
        boxShadow: [
          BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25.0),
        ),
        child: BottomNavigationBar(
          selectedFontSize: 12.0,
          onTap: (index) {
            provider.setBnbIndex(index);
            print(provider.bnbIndex);
          },
          selectedItemColor: Colors.green,
          unselectedItemColor: Colors.black54,
          currentIndex: provider.bnbIndex,
          showUnselectedLabels: false,
          type: BottomNavigationBarType.shifting,
          items: [
            BottomNavigationBarItem(
              title: Text(
                'شارژکیف پول',
                style: TextStyle(fontSize: 11),
              ),
              icon: SvgPicture.asset(
                bnbIcon[0],
                color: provider.bnbIndex == 0 ? COLOR_MESSAGE : Colors.black54,
                width: 25.0,
                height: 25.0,
              ),
            ),
            BottomNavigationBarItem(
              title: Text(
                'موجودی',
                style: TextStyle(fontSize: 11),
              ),
              icon: SvgPicture.asset(
                bnbIcon[1],
                color: provider.bnbIndex == 1 ? COLOR_MESSAGE : Colors.black54,
                width: 25.0,
                height: 25.0,
              ),
            ),
            BottomNavigationBarItem(
              title: Text(
                'شاندیز من',
                style: TextStyle(fontSize: 11),
              ),
              icon: SvgPicture.asset(
                bnbIcon[2],
                color: provider.bnbIndex == 2 ? COLOR_MESSAGE : Colors.black54,
                width: 25.0,
                height: 25.0,
              ),
            ),
            BottomNavigationBarItem(
              title: Text(
                'صورتحساب',
                style: TextStyle(fontSize: 11),
              ),
              icon: SvgPicture.asset(
                bnbIcon[3],
                color: provider.bnbIndex == 3 ? COLOR_MESSAGE : Colors.black54,
                width: 25.0,
                height: 25.0,
              ),
            ),
            BottomNavigationBarItem(
              title: FittedBox(
                child: Text(
                  'پرداخت بدون کارت',
                  style: TextStyle(fontSize: 11),
                ),
              ),
              icon: SvgPicture.asset(
                bnbIcon[4],
                color: provider.bnbIndex == 4 ? COLOR_MESSAGE : Colors.black54,
                width: 25.0,
                height: 25.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
