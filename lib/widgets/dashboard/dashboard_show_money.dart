import 'package:flutter/material.dart';
import '../utils.dart';

class ShowMoney extends StatelessWidget {
  const ShowMoney({
    @required this.cardText,
    @required this.numberMoney,
    @required this.rialText,
  });

  final String cardText;
  final String numberMoney;
  final String rialText;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 7.0),
          decoration: BoxDecoration(
            color: Colors.green[900],
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Row(
            children: [
              Row(
                children: [
                  Icon(
                    Icons.credit_card,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  Text(
                    cardText,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
              SizedBox(
                width: 5.0,
              ),
              Row(
                children: [
                  Text(
                    numberMoney.toMoneyFormat().toFarsi(),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  Text(
                    rialText,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
