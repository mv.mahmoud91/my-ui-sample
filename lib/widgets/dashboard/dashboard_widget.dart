import 'package:flutter/material.dart';
import '../../models/api/main/dashboard/dashboard_item.dart';
import '../dashboard/dashboard_item.dart';
import '../utils.dart';

class DashBoardWidget extends StatelessWidget {
  final List<Dashboard> loadedDashboard = [
    Dashboard(title: 'شورای شهر', iconUrl: 'assets/images/shora-shahrd.svg'),
    Dashboard(title: 'سامانه 137', iconUrl: 'assets/images/samane137.svg'),
    Dashboard(title: 'خدمات شهروندی', iconUrl: 'assets/images/shahrdari.svg'),
    Dashboard(title: 'شهرداری', iconUrl: 'assets/images/shahrdari.svg'),
    Dashboard(title: 'آرامستان', iconUrl: 'assets/images/aramestan.svg'),
    Dashboard(title: 'اماکن دیدنی', iconUrl: 'assets/images/aramestan.svg'),
    Dashboard(title: 'مدیریت کارت', iconUrl: 'assets/images/manage-card.svg'),
    Dashboard(title: 'پذیرندگان', iconUrl: 'assets/images/pazirandegan.svg'),
    Dashboard(title: 'گنج یاب', iconUrl: 'assets/images/ganj-yab.svg'),
    Dashboard(title: 'نقشه گنج', iconUrl: 'assets/images/map-ganj.svg'),
    Dashboard(title: 'برنده ها', iconUrl: 'assets/images/winners.svg'),
    Dashboard(title: 'خرید شارژ', iconUrl: 'assets/images/charge-buy.svg'),
    Dashboard(
        title: 'خرید بسته اینترنت', iconUrl: 'assets/images/buy-internet.svg'),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage('assets/images/background-dashboard.png'),
        ),
      ),
      child: GridView.builder(
        itemCount: loadedDashboard.length,
        itemBuilder: (ctx, index) {
          var dashboardItem = loadedDashboard[index];
          return Column(
            children: [
              SizedBox(
                height: 15.0,
              ),
              DashboardItem(
                iconUrl: dashboardItem.iconUrl,
                title: dashboardItem.title.toFarsi(),
              ),
            ],
          );
        },
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          // mainAxisSpacing: 10.0,
          crossAxisCount: 3,
          childAspectRatio: 1.0,
        ),
      ),
    );
  }
}
