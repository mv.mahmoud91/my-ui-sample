import 'package:flutter/material.dart';
import '../dashboard/dashboard_slider_widget.dart';
import '../dashboard/dashboard_show_money.dart';

class TopSlider extends StatelessWidget {
  TopSlider({@required this.money, @required this.slider});

  final String money;
  final List<String> slider;

  @override
  Widget build(BuildContext context) {
    final String cardText = 'کیف پول :';
    final String rialText = 'ریال';
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height * .3,
      child: Stack(
        children: [
          Container(
            height: size.height * .3,
            child: SliderWidget(
              slider: slider,
              size: size.height * .3,
            ),
          ),
          Positioned(
            bottom: -1,
            child: Container(
              height: 70.0,
              width: size.width,
              color: Colors.transparent,
              child: CustomPaint(
                painter: CurvePainter(),
              ),
            ),
          ),
          Positioned(
            bottom: 15,
            right: 0,
            left: 0,
            child: ShowMoney(
                cardText: cardText, numberMoney: money, rialText: rialText),
          ),
        ],
      ),
    );
  }
}

class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = Colors.white;
    paint.style = PaintingStyle.fill;

    var path = Path();

    path.moveTo(0, 0);

    path.quadraticBezierTo(
        size.width * 0.5, size.height * 0.99999, size.width, 0);

    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
