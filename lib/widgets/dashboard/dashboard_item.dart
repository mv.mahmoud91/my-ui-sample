import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DashboardItem extends StatelessWidget {
  final String iconUrl;
  final String title;

  DashboardItem({@required this.iconUrl, @required this.title});

  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        RotationTransition(
          turns: AlwaysStoppedAnimation(45 / 360),
          child: SizedBox(
            width: size.width * .16,
            height: size.width * .16,
            child: Container(
              padding: EdgeInsets.all(15.0),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 7,
                    offset: Offset(0, 3),
                  ),
                ],
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: RotationTransition(
                turns: AlwaysStoppedAnimation(-45 / 360),
                child: SvgPicture.asset(
                  iconUrl,
                  fit: BoxFit.contain,
                  width: 35.0,
                  height: 35.0,
                  color: Colors.green[900],
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 15.0,
        ),
        Text(
          title,
          style: TextStyle(fontSize: 11, fontWeight: FontWeight.normal),
        ),
      ],
    );
  }
}
