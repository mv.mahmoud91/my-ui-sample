import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class SliderWidget extends StatefulWidget {
  final double size;
  final List<String> slider;

  SliderWidget({this.size, @required this.slider});

  @override
  _SliderWidgetState createState() => _SliderWidgetState();
}

class _SliderWidgetState extends State<SliderWidget> {
  List<String> imageUrl;
  PageController _controller;
  double _currentPage = 0;
  bool goReverse = false;

  @override
  void initState() {
    super.initState();
    _controller = PageController(initialPage: 0, viewportFraction: 1.0);
    _controller.addListener(() {
      setState(() {
        _currentPage = _controller.page;
      });
    });
    automateSliderUp();
  }

  void automateSliderUp() {
    Timer.periodic(Duration(seconds: 5), (Timer timer) {
      if (_currentPage < widget.slider.length - 1) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }
      if (_controller.hasClients) {
        _controller.animateToPage(
          _currentPage.toInt(),
          duration: Duration(milliseconds: 350),
          curve: Curves.easeIn,
        );
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.size,
      child: PageView.builder(
        controller: _controller,
        itemCount: widget.slider.length,
        itemBuilder: (ctx, index) {
          return ExtendedImage.network(
            widget.slider[index],
            fit: BoxFit.fill,
            cache: true,
          );
        },
      ),
    );
  }
}
