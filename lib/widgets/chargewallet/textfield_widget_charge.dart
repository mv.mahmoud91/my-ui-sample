import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../constants/constants.dart';
import '../../widgets/utils.dart';

class TextFieldWidgetCharge extends StatelessWidget {
  TextFieldWidgetCharge({
    @required this.text,
    @required this.length,
    @required this.hintText,
    @required this.obSecure,
    @required this.textInputType,
    @required this.textInputFormatter,
    @required this.textFunction,
    @required this.textEditingController,
    @required this.submitted,
  });

  final String text;
  final bool obSecure;
  final String hintText;
  final int length;
  final TextInputType textInputType;
  final FilteringTextInputFormatter textInputFormatter;
  final FormFieldValidator<String> textFunction;
  final TextEditingController textEditingController;
  final Function submitted;

  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: const EdgeInsets.symmetric(vertical: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            text,
            style: acceptStyle(context),
          ),
          SizedBox(
            height: 5.0,
          ),
          Container(
            // height: 50.0,
            alignment: Alignment.center,
            child: TextFormField(
              onFieldSubmitted: submitted,
              controller: textEditingController,
              validator: textFunction,
              inputFormatters: [CurrencyInputFormatter()],
              maxLength: length,
              style: chargeStyle(context),
              keyboardType: textInputType,
              obscureText: obSecure,
              textAlign: TextAlign.start,
              showCursor: true,
              decoration: InputDecoration(
                errorStyle: TextStyle(
                  fontSize: 11.0,
                ),
                counterText: '',
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                hintText: hintText,
                border: OutlineInputBorder(
                  borderSide: BorderSide(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
