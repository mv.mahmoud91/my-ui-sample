import 'package:flutter/material.dart';
import '../../widgets/utils.dart';

class ChargeSelectAmountContainer extends StatelessWidget {
  ChargeSelectAmountContainer({
    @required this.rial,
    @required this.isSelected,
  });

  final String rial;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    String _rial = 'ریال';
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      height: double.maxFinite,
      margin: const EdgeInsets.all(5.0),
      decoration: BoxDecoration(
        color: isSelected ? Colors.yellow.withOpacity(.2) : Colors.transparent,
        borderRadius: BorderRadius.circular(10.0),
        border: Border.all(color: Colors.orange),
      ),
      child: Text(
        rial.toMoneyFormat() + ' ' + _rial,
        style: TextStyle(fontSize: size.width * .03),
      ),
    );
  }
}
