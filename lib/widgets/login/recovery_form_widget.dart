import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import '../../provider/login_screen/provider_login.dart';
import '../../constants/constants.dart';
import '../login/text_field_widget.dart';

class RecoveryFormWidget extends StatefulWidget {
  RecoveryFormWidget({
    @required this.companyName,
  });

  final String companyName;

  @override
  _RecoveryFormWidgetState createState() => _RecoveryFormWidgetState();
}

class _RecoveryFormWidgetState extends State<RecoveryFormWidget> {
  TextEditingController controllerUserName;
  @override
  void initState() {
    controllerUserName = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    controllerUserName.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 40.0),
      color: Colors.white,
      child: Column(
        children: [
          FittedBox(
            child: Text(
              USER_RECOVERY_PASSWORD_MESSAGE,
              textWidthBasis: TextWidthBasis.longestLine,
            ),
          ),
          TextFieldWidget(
            textDirection: TextDirection.rtl,
            textFunction: null,
            textEditingController: controllerUserName,
            textInputFormatter: FilteringTextInputFormatter.digitsOnly,
            isEnabled: true,
            text: USER_NAME_TEXT,
            length: 10,
            hintText: '',
            obSecure: false,
            textInputType: TextInputType.number,
          ),
          Container(
            width: double.infinity,
            child: RecoveryButton(
              userName: controllerUserName,
              size: size,
              context: context,
            ),
          ),
          Divider(
            color: Colors.grey[400],
            thickness: 1.0,
          ),
          FittedBox(
            child: GestureDetector(
              onTap: () {
                Provider.of<LoginProvider>(context, listen: false)
                    .signUpIndex();
              },
              child: Text(
                USER_SIGN_UP_MESSAGE,
                style: TextStyle(fontSize: 12.0),
              ),
            ),
          ),
          Text(
            widget.companyName,
            style: TextStyle(fontSize: 9.0),
          ),
        ],
      ),
    );
  }
}

class RecoveryButton extends StatelessWidget {
  RecoveryButton({
    this.userName,
    this.size,
    this.context,
  });

  final Size size;
  final TextEditingController userName;
  final BuildContext context;

  @override
  Widget build(BuildContext recoveryContext) {
    return Consumer<LoginProvider>(
      builder: (recoveryContext, value, _) {
        return RaisedButton(
          color: COLOR_MESSAGE,
          onPressed: () {
            if (userName.text.length < 10) {
              Alert(
                  closeFunction: () {},
                  context: context,
                  image: Image.asset(
                    'assets/images/danger.png',
                    width: 50.0,
                    height: 50.0,
                    fit: BoxFit.contain,
                    color: Colors.red,
                  ),
                  style: AlertStyle(
                    isOverlayTapDismiss: false,
                    isCloseButton: false,
                    titleStyle: TextStyle(
                        fontSize: 14.0, fontWeight: FontWeight.normal),
                  ),
                  title: 'وارد کردن نام کاربری الزامی می باشد',
                  buttons: [
                    DialogButton(
                      color: COLOR_MESSAGE,
                      onPressed: () => Navigator.pop(context),
                      child: Text(
                        "بستن",
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    )
                  ]).show();
            } else {
              value.connect();
              value.getRecoveryUserName(userName.text.trim());
              value.onLoadingRecovery(context);
              print(userName.text);
            }
          },
          child: Text(
            'بازیابی کلمه عبور',
            style:
                TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
          ),
        );
      },
    );
  }
}
