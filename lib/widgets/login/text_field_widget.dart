import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../constants/constants.dart';

class TextFieldWidget extends StatelessWidget {
  const TextFieldWidget({
    @required this.text,
    @required this.length,
    @required this.hintText,
    @required this.obSecure,
    @required this.textInputType,
    @required this.textInputFormatter,
    @required this.textFunction,
    @required this.textEditingController,
    @required this.isEnabled,
    @required this.textDirection,
  });

  final String text;
  final bool obSecure;
  final String hintText;
  final int length;
  final TextInputType textInputType;
  final FilteringTextInputFormatter textInputFormatter;
  final Function textFunction;
  final TextEditingController textEditingController;
  final bool isEnabled;
  final TextDirection textDirection;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            text,
            style: loginStyle(context),
          ),
          SizedBox(
            height: 5.0,
          ),
          Container(
            alignment: Alignment.center,
            child: TextFormField(
              textDirection: textDirection,
              enabled: isEnabled,
              controller: textEditingController,
              validator: isEnabled ? textFunction : null,
              inputFormatters: <TextInputFormatter>[textInputFormatter],
              maxLength: length,
              style: loginStyle(context),
              keyboardType: textInputType,
              obscureText: obSecure,
              textAlign: TextAlign.start,
              showCursor: true,
              decoration: InputDecoration(
                errorStyle: TextStyle(
                  fontSize: 11.0,
                ),
                counterText: '',
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                hintText: hintText,
                border: OutlineInputBorder(
                  borderSide: BorderSide(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
