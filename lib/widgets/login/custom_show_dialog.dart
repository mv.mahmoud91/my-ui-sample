import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../constants/constants.dart';

enum DialogType {
  success,
  danger,
  info,
  warning,
}

class ADialog extends StatelessWidget {
  final bool titleInHeader;
  final String title, svgPath, acceptText, cancelText;
  final Widget child;
  Color alertColor;
  int alertColorShadow;
  final Function onAccept;
  final Function onCancel;

  ADialog({
    this.titleInHeader = false,
    this.title,
    this.child,
    this.svgPath = 'assets/svg/question.svg',
    this.acceptText,
    this.cancelText,
    this.onAccept,
    this.onCancel,
  });

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: dialogContent(context),
      ),
    );
  }

  dialogContent(BuildContext context) {
    return Container(
//      color: Colors.purple,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        decoration: new BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          border: Border.all(color: alertColor, width: 2),
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              offset: const Offset(0.0, 10.0),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(4),
          child: Stack(
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    height: 50,
                    decoration: BoxDecoration(
                      color: alertColor,
                      borderRadius: BorderRadius.circular(11),
                    ),
                    child: Row(
                      children: <Widget>[
                        Text(
                          title,
                          style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: child,
                  ),
                  Row(
                    children: <Widget>[
                      acceptText != null
                          ? Align(
                              alignment: Alignment.bottomRight,
                              child: FlatButton(
                                splashColor: Color(alertColorShadow),
                                onPressed: () {
//                        Navigator.of(context).pop(); // To close the dialog
                                  onAccept(context);
                                },
                                child: Text(
                                  acceptText,
                                  style: TextStyle(color: alertColor),
                                ),
                              ),
                            )
                          : SizedBox(),
                      cancelText != null
                          ? Align(
                              alignment: Alignment.bottomRight,
                              child: FlatButton(
                                onPressed: () {
//                        Navigator.of(context).pop(); // To close the dialog
                                  onCancel(context);
                                },
                                child: Text(cancelText),
                              ),
                            )
                          : SizedBox(),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AModal extends StatelessWidget {
  final Widget child;
  final EdgeInsets padding;
  AModal(
      {this.child,
      this.padding = const EdgeInsets.symmetric(vertical: 10, horizontal: 10)});
  @override
  Widget build(BuildContext context) {
    double size = MediaQuery.of(context).size.width * .5;
    return Dialog(
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: Container(
        padding: padding,
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'تایید قوانین',
                  style: loginStyle(context),
                ),
              ],
            ),
            Stack(
              children: [
                Divider(
                  color: Colors.grey,
                  height: 20.0,
                  thickness: 1.0,
                ),
                Padding(
                  padding: EdgeInsets.only(left: size),
                  child: Divider(
                    color: COLOR_MESSAGE,
                    height: 20.0,
                    thickness: 1.0,
                  ),
                ),
              ],
            ),
            Container(
              padding: padding,
              color: Colors.white,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(0), child: child),
            ),
            Center(
              child: Container(
                height: 30.0,
                child: RaisedButton(
                  color: COLOR_MESSAGE,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    'بستن',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 11.0,
                        fontWeight: FontWeight.normal),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
