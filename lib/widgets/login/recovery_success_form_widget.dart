import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import '../../provider/login_screen/provider_login.dart';
import '../../constants/constants.dart';
import '../login/text_field_widget.dart';

class RecoverySuccessFormWidget extends StatefulWidget {
  RecoverySuccessFormWidget({
    @required this.companyName,
  });

  final String companyName;

  @override
  _RecoverySuccessFormWidgetState createState() =>
      _RecoverySuccessFormWidgetState();
}

class _RecoverySuccessFormWidgetState extends State<RecoverySuccessFormWidget> {
  TextEditingController controllerPassword;
  @override
  void initState() {
    controllerPassword = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    controllerPassword.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 40.0),
      color: Colors.white,
      child: Column(
        children: [
          SuccessMessages(),
          TextFieldWidget(
            textDirection: TextDirection.ltr,
            textFunction: null,
            textEditingController: controllerPassword,
            textInputFormatter: FilteringTextInputFormatter.digitsOnly,
            isEnabled: true,
            text: 'کلمه عبور :',
            length: 10,
            hintText: '',
            obSecure: true,
            textInputType: TextInputType.number,
          ),
          Container(
            width: double.infinity,
            child: LoginButton(
              context: context,
              size: size,
              password: controllerPassword,
            ),
          ),
          Divider(
            color: Colors.grey[400],
            thickness: 1.0,
          ),
          FittedBox(
            child: GestureDetector(
              onTap: () {
                Provider.of<LoginProvider>(context, listen: false)
                    .recoveryIndex();
              },
              child: Text(
                USER_RECOVERY_AGAIN_TEXT,
                style: TextStyle(fontSize: 12.0),
              ),
            ),
          ),
          Text(
            widget.companyName,
            style: TextStyle(fontSize: 9.0),
          ),
        ],
      ),
    );
  }
}

class SuccessMessages extends StatelessWidget {
  @override
  Widget build(BuildContext success) {
    return Consumer<LoginProvider>(
      builder: (success, value, _) {
        return Column(
          children: [
            FittedBox(
              child: Text(
                value.recoverySuccessText,
                textWidthBasis: TextWidthBasis.longestLine,
              ),
            ),
            Text(
              value.showMobile,
              textDirection: TextDirection.ltr,
              textWidthBasis: TextWidthBasis.longestLine,
            ),
          ],
        );
      },
    );
  }
}

class LoginButton extends StatelessWidget {
  LoginButton({
    this.password,
    this.size,
    this.context,
  });

  final Size size;
  final TextEditingController password;
  final BuildContext context;

  @override
  Widget build(BuildContext loginContext) {
    return Consumer<LoginProvider>(
      builder: (loginContext, value, _) {
        return RaisedButton(
          color: COLOR_MESSAGE,
          onPressed: () {
            bool isConnected = value.isConnected ?? true;
            if (isConnected) {
              value.connect();
              value.nationalCodeInput(value.recoveryUserName);
              value.phoneNumberInput(password.text.trim());

              value.onLoadingLogin(loginContext);
            } else {
              value.isConnect(loginContext);
            }
            print(password.text);
          },
          child: Text(
            'ورود',
            style:
                TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
          ),
        );
      },
    );
  }
}
