import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import '../../widgets/login/custom_show_dialog.dart';
import '../../provider/login_screen/provider_login.dart';
import '../../provider/login_screen/provider_validate.dart';
import '../../provider/login_screen/provider_checkbox.dart';
import '../../constants/constants.dart';
import '../login/text_field_widget.dart';
import '../utils.dart';

class SignUpFormWidget extends StatefulWidget {
  SignUpFormWidget({
    @required this.companyName,
  });

  final String companyName;

  @override
  _SignUpFormWidgetState createState() => _SignUpFormWidgetState();
}

class _SignUpFormWidgetState extends State<SignUpFormWidget> {
  String _rules = '';
  TextEditingController textControllerCode;
  TextEditingController textControllerMobile = TextEditingController();

  @override
  void initState() {
    final provider = Provider.of<LoginProvider>(context, listen: false);
    super.initState();
    textControllerCode = TextEditingController();
    textControllerMobile = TextEditingController();
    _rules = provider.rules;
  }

  @override
  void dispose() {
    textControllerCode.dispose();
    textControllerMobile.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final LoginProvider providerSignUp =
        Provider.of<LoginProvider>(context, listen: false);
    print('rebuild');

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 40.0),
      color: Colors.white,
      child: Column(
        children: [
          Form(
            key: _formKey,
            child: Column(
              children: [
                TextFieldWidget(
                  textDirection: TextDirection.rtl,
                  isEnabled: true,
                  textEditingController: textControllerCode,
                  textFunction: _validateCode,
                  textInputFormatter: FilteringTextInputFormatter.digitsOnly,
                  text: USER_CODE_TEXT,
                  length: 10,
                  hintText: '',
                  obSecure: false,
                  textInputType: TextInputType.number,
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextFieldWidget(
                  textDirection: TextDirection.rtl,
                  isEnabled: true,
                  textEditingController: textControllerMobile,
                  textFunction: _validateMobile,
                  textInputFormatter: FilteringTextInputFormatter.digitsOnly,
                  text: USER_PHONE_NUMBER_TEXT,
                  length: 11,
                  hintText: '09000000000',
                  obSecure: false,
                  textInputType: TextInputType.number,
                ),
              ],
            ),
          ),
          Container(
            height: 30.0,
            child: Row(
              children: [
                Expanded(
                  flex: 3,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      CheckBox(),
                      FittedBox(
                        child: Text(
                          USER_RULES_TEXT,
                          style: loginStyle(context),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () {
                      _inShowCustomDialog(context, _rules);
                    },
                    child: FittedBox(
                      child: Text(
                        USER_SHOW_RULES_TEXT,
                        style: loginStyle(context),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            child: RaisedButton(
              color: COLOR_MESSAGE,
              onPressed: () {
                providerSignUp.connect();
                bool isConnected = providerSignUp.isConnected ?? true;
                final s = DateTime.now().millisecondsSinceEpoch;
                if (s - 1599762632569 >= 3600000) {
                  print('یک ساعت شد');
                } else {
                  print('یک ساعت نشد');
                }

                print(isConnected);
                if (isConnected) {
                  bool isChecked =
                      Provider.of<CheckBoxProvider>(context, listen: false)
                          .checkBoxSignUp;
                  if (!isChecked && _formKey.currentState.validate() == false) {
                    _formKey.currentState.validate();
                  }
                  if (isChecked && _formKey.currentState.validate() == false) {
                    _formKey.currentState.validate();
                  }
                  if (!isChecked && _formKey.currentState.validate() == true) {
                    providerSignUp
                        .nationalCodeInput(textControllerCode.text.trim());
                    providerSignUp
                        .phoneNumberInput(textControllerMobile.text.trim());
                    print(providerSignUp.nationalCode);

                    print(providerSignUp.phoneNumber);
                    _onAlertWithStylePressed(context, size);
                  }
                  if (isChecked && _formKey.currentState.validate() == true) {
                    providerSignUp
                        .nationalCodeInput(textControllerCode.text.trim());
                    providerSignUp
                        .phoneNumberInput(textControllerMobile.text.trim());
                    providerSignUp.onSignUp(context);
                    print(providerSignUp.nationalCode);
                    print(providerSignUp.phoneNumber);
                    print('Log in');
                  }
                } else {
                  providerSignUp.isConnect(context);
                }
              },
              child: Text(
                USER_SIGN_UP_TEXT,
                style: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.normal),
              ),
            ),
            //  !!!!!!!!!!!!!!!!!
          ),
          Divider(
            color: Colors.grey[400],
            thickness: 1.0,
          ),
          FittedBox(
            child: GestureDetector(
              onTap: () {
                Provider.of<LoginProvider>(context, listen: false).loginIndex();
              },
              child: Text(
                USER_SIGN_IN_MESSAGE,
                style: TextStyle(fontSize: 12.0),
              ),
            ),
          ),
          Text(
            widget.companyName,
            style: TextStyle(fontSize: 9.0),
          ),
        ],
      ),
    );
  }

  void validate(
      TextEditingController textControllerCode, BuildContext context) {
    if (_validateCode(textControllerCode.text).length > 0) {
      Provider.of<ValidateProvider>(context, listen: false).notCorrect();
    } else if (_validateCode(textControllerCode.text).length == null) {
      Provider.of<ValidateProvider>(context, listen: false).isCorrect();
    }
  }

  String _validateMobile(String value) {
    if (value.isEmpty) {
      return 'وارد کردن شماره همراه الزامی می باشد';
    } else if (!value.startsWith('09') || value.length < 11) {
      return 'شماره همراه وارد شده معتبر نمی باشد';
    } else {
      return null;
    }
  }

  String _validateCode(String value) {
    if (value.isEmpty) {
      return 'وارد کردن کد ملی الزامی می باشد';
    } else if (validateNationalCode(value) == false) {
      return 'کد ملی معتبر نمی باشد';
    } else {
      return null;
    }
  }

  void _inShowCustomDialog(BuildContext context, String rules) {
    showGeneralDialog(
      barrierColor: Colors.black54,
      transitionDuration: Duration(milliseconds: 200),
      barrierDismissible: true,
      barrierLabel: '',
      context: context,
      transitionBuilder: (context, a1, a2, widget) {
        final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
        var size = MediaQuery.of(context).size;
        return Transform(
          transform: Matrix4.translationValues(0.0, curvedValue * 200, 0.0),
          child: AModal(
            child: Container(
              width: size.width * .99,
              height: size.height * .5,
              child: SingleChildScrollView(
                child: Text(
                  rules,
                  style: loginStyle(context),
                ),
              ),
            ),
          ),
        );
      },
      pageBuilder: (BuildContext context, Animation animation,
          Animation secondaryAnimation) {
        return;
      },
    );
  }
}

class CheckBox extends StatelessWidget {
  const CheckBox({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    print('provider checkbox sign up');
    return Consumer<CheckBoxProvider>(builder: (ctx, value, _) {
      return Checkbox(
        onChanged: (newValue) {
          value.checkBoxValueSignUp(newValue);
        },
        value: value.checkBoxSignUp,
        activeColor: Colors.grey[700],
      );
    });
  }
}

_onAlertWithStylePressed(context, Size size) {
  Alert(
      closeFunction: () {},
      context: context,
      image: Image.asset(
        'assets/images/danger.png',
        width: 50.0,
        height: 50.0,
        fit: BoxFit.contain,
        color: Colors.red,
      ),
      style: AlertStyle(
        isOverlayTapDismiss: false,
        isCloseButton: false,
        titleStyle: TextStyle(fontSize: 12.0, fontWeight: FontWeight.normal),
      ),
      title: "لطفا قوانین را تایید نمایید",
      content: Column(
        children: <Widget>[],
      ),
      buttons: [
        DialogButton(
          color: COLOR_MESSAGE,
          onPressed: () => Navigator.pop(context),
          child: Text(
            "بستن",
            style: TextStyle(color: Colors.white, fontSize: 14),
          ),
        )
      ]).show();
}
