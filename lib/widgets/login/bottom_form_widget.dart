import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../provider/login_screen/provider_login.dart';
import '../../widgets/login/recovery_success_form_widget.dart';
import '../../widgets/login/login_form_widget.dart';
import '../../widgets/login/recovery_form_widget.dart';
import '../../constants/constants.dart';
import '../../widgets/login/sign_up_form_widget.dart';

class LoginBottomForm extends StatelessWidget {
  const LoginBottomForm({
    @required this.companyName,
  });

  final String companyName;

  @override
  Widget build(BuildContext context) {
    int index = Provider.of<LoginProvider>(context).pageIndex;
    bool login = Provider.of<LoginProvider>(context).isLoginStart;
    print('page Index = ' + index.toString());
    Size size = MediaQuery.of(context).size;
    List<Widget> loginFormWidgets = [
      SignUpFormWidget(
        companyName: companyName,
      ),
      LoginFormWidget(
        companyName: companyName,
      ),
      RecoveryFormWidget(
        companyName: companyName,
      ),
      RecoverySuccessFormWidget(
        companyName: companyName,
      ),
    ];
    return Container(
      width: double.infinity,
      height: size.height * .71,
      color: COLOR_MESSAGE,
      child: Container(
        width: double.infinity,
        height: size.height,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 10.0),
              child: Image.asset(
                'assets/images/logo_1.png',
                width: size.width * .3,
              ),
            ),
            loginFormWidgets[login ? 1 : index ?? 0],
          ],
        ),
      ),
    );
  }
}
