import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';
import 'package:sample_company/provider/dashboard_screen/dashboard_screen_provider.dart';
import '../../provider/login_screen/provider_login.dart';
import '../../provider/login_screen/provider_login_validate.dart';
import '../../widgets/shared_preference.dart';
import '../../constants/constants.dart';
import '../login/text_field_widget.dart';
import '../../provider/login_screen/provider_checkbox.dart';
import '../utils.dart';

class LoginFormWidget extends StatefulWidget {
  LoginFormWidget({
    @required this.companyName,
  });

  final String companyName;

  @override
  _LoginFormWidgetState createState() => _LoginFormWidgetState();
}

class _LoginFormWidgetState extends State<LoginFormWidget> {
  TextEditingController controllerUserName;
  TextEditingController controllerPassword;
  bool _isFingerPrint = false;
  BiometricType _biometricType = BiometricType.fingerprint;

  @override
  void initState() {
    final LoginProvider provider =
        Provider.of<LoginProvider>(context, listen: false);
    super.initState();
    controllerUserName = TextEditingController(text: provider.saveUserName);
    controllerPassword = TextEditingController(text: provider.savePassword);
    LocalAuthentication().getAvailableBiometrics().then((result) {
      _biometricType = result.first;
    });
    LocalAuthentication().canCheckBiometrics.then((value) {
      setState(() {
        _isFingerPrint = value;
      });
    });
  }

  @override
  void dispose() {
    controllerUserName?.dispose();
    controllerPassword?.dispose();
    super.dispose();
  }

  final _loginFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    print('loginform rebuild');
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 40.0),
      color: Colors.white,
      child: Column(
        children: [
          Form(
            key: _loginFormKey,
            child: Column(
              children: [
                TextFieldWidget(
                  textDirection: TextDirection.rtl,
                  isEnabled: true,
                  textEditingController: controllerUserName,
                  textFunction: _validateUserName,
                  textInputFormatter: FilteringTextInputFormatter.digitsOnly,
                  text: USER_NAME_TEXT,
                  length: 10,
                  hintText: '',
                  obSecure: false,
                  textInputType: TextInputType.number,
                ),
                SizedBox(
                  height: 10.0,
                ),
                TextFieldWidget(
                  textDirection: TextDirection.ltr,
                  isEnabled: true,
                  textFunction: _validatePassword,
                  textInputFormatter:
                      FilteringTextInputFormatter.singleLineFormatter,
                  textEditingController: controllerPassword,
                  text: USER_PASSWORD_TEXT,
                  length: 30,
                  hintText: '',
                  obSecure: true,
                  textInputType: TextInputType.text,
                ),
              ],
            ),
          ),
          Container(
            height: size.height * .06,
            child: Row(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    CheckBox(),
                    Text(
                      USER_SAVE_PASSWORD_TEXT,
                      style: loginStyle(context),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(2.0),
            height: size.height * .06,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  flex: 27,
                  child: SizedBox(
                    height: double.infinity,
                    child: LoginButton(
                      size: size,
                      loginFormKey: _loginFormKey,
                      userName: controllerUserName,
                      password: controllerPassword,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(),
                ),
                Expanded(
                  flex: 5,
                  child: ButtonTheme(
                    padding: const EdgeInsets.all(1.0),
                    child:
                        FingerPrintButton(controllerUserName, _isFingerPrint),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey[400],
            thickness: 1,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FittedBox(
                child: GestureDetector(
                  onTap: () {
                    Provider.of<LoginProvider>(context, listen: false)
                        .recoveryIndex();
                  },
                  child: Text(
                    USER_RECOVERY_PASSWORD_TEXT,
                    style: TextStyle(fontSize: 12.0),
                  ),
                ),
              ),
              FittedBox(
                child: GestureDetector(
                  onTap: () {
                    Provider.of<LoginProvider>(context, listen: false)
                        .signUpIndex();
                  },
                  child: Text(
                    USER_JOIN_CLUB_TEXT,
                    style: TextStyle(fontSize: 12.0),
                  ),
                ),
              ),
            ],
          ),
          FittedBox(
            child: Text(
              widget.companyName ?? '',
              style: TextStyle(fontSize: 9.0),
            ),
          ),
        ],
      ),
    );
  }

  void validate(
      TextEditingController textControllerCode, BuildContext context) {
    if (_validateUserName(textControllerCode.text).length > 0) {
      Provider.of<LoginProviderValidate>(context, listen: false).notCorrect();
    } else if (_validateUserName(textControllerCode.text).length == null) {
      Provider.of<LoginProviderValidate>(context, listen: false).isCorrect();
    }
  }

  String _validateUserName(String value) {
    if (value.isEmpty) {
      return 'وارد کردن نام کاربری الزامی می باشد';
    } else if (validateNationalCode(value) == false) {
      return 'نام کاربری وارد شده معتبر نمی باشد';
    } else {
      return null;
    }
  }

  String _validatePassword(String value) {
    if (value.isEmpty) {
      return 'وارد کردن کلمه عبور الزامی می باشد';
    } else {
      return null;
    }
  }
}

class FingerPrintButton extends StatelessWidget {
  const FingerPrintButton(this.controllerUserName, this.isFingerPrint);

  final TextEditingController controllerUserName;
  final bool isFingerPrint;

  @override
  Widget build(BuildContext fingerContext) {
    return Consumer<LoginProvider>(builder: (fingerContext, value, _) {
      return RaisedButton(
        color: isFingerPrint ? COLOR_MESSAGE : Colors.grey[300],
        onPressed: () =>
            isFingerPrint ? _fingerPrint(fingerContext, value) : null,
        child: Image.asset(
          'assets/images/ic_fingerprint.png',
          fit: BoxFit.contain,
        ),
      );
    });
  }

  _fingerPrint(fingerContext, LoginProvider value) async {
    var localAuth = LocalAuthentication();
    try {
      bool didAuthenticate = await localAuth.authenticateWithBiometrics(
          localizedReason: 'انگشت خود را روی حسگر اثر انگشت قرار دهید',
          androidAuthStrings: AndroidAuthMessages(
              signInTitle: 'احراز هویت با اثر انگشت', fingerprintHint: ''),
          stickyAuth: false,
          useErrorDialogs: false);
      if (didAuthenticate) {
        String fingerUsername = await getStringPrefs('fingerUserName');
        String fingerPassword = await getStringPrefs('fingerPassword');
        String decodedUserName, decodedPassword;
        if (fingerUsername != 'false' && fingerUsername != '') {
          decodedUserName = utf8.decode(base64Url.decode(fingerUsername));
          decodedPassword = utf8.decode(base64Url.decode(fingerPassword));
        }
        if (fingerUsername != null &&
            fingerUsername != 'false' &&
            decodedUserName == controllerUserName.text) {
          value.connect();
          value.onFingerPrint(fingerContext, decodedUserName, decodedPassword);
        } else {
          fingerAlert(fingerContext, FINGER_PRINT_TEXT, COLOR_MESSAGE);
        }
      }
    } on PlatformException catch (e) {
      print(e);
    }
  }
}

class LoginButton extends StatelessWidget {
  LoginButton({
    this.loginFormKey,
    this.userName,
    this.password,
    this.size,
  });

  final GlobalKey<FormState> loginFormKey;
  final Size size;
  final TextEditingController userName;
  final TextEditingController password;

  @override
  Widget build(BuildContext loginContext) {
    return Consumer<LoginProvider>(
      builder: (loginContext, value, _) {
        return RaisedButton(
          color: COLOR_MESSAGE,
          onPressed: () {
            Provider.of<ProviderDashBoardScreen>(loginContext, listen: false)
                .setBnbIndex(2);
            _login(loginContext, value);
          },
          child: Text(
            'ورود',
            style:
                TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
          ),
        );
      },
    );
  }

  _login(loginContext, LoginProvider value) {
    bool saved = Provider.of<CheckBoxProvider>(loginContext, listen: false)
        .checkBoxLogin;
    print('checkBox================>' + saved.toString());
    value.isSavedInput(saved);
    print(value.isConnected);
    bool isConnected = value.isConnected ?? true;
    if (isConnected) {
      if (!loginFormKey.currentState.validate()) {
        loginFormKey.currentState.validate();
      } else {
        value.connect();
        value.nationalCodeInput(userName.text.trim());
        value.phoneNumberInput(password.text.trim());

        print(value.nationalCode);
        print(value.phoneNumber);
        value.onLoadingLogin(loginContext);
      }
    } else {
      value.isConnect(loginContext);
    }
    print(userName.text);
    print(password.text);
  }
}

class CheckBox extends StatelessWidget {
  @override
  Widget build(BuildContext check) {
    print('provider checkbox login');
    return Consumer<CheckBoxProvider>(builder: (ctx, value, _) {
      return Checkbox(
        onChanged: (newValue) async {
          value.checkBoxValueLogin(newValue);
          String s = await getStringPrefs('savePassword');
          print(s);
          // _checkBox.checkBoxValueSignUp();
        },
        value: value.checkBoxLogin,
        activeColor: Colors.grey[700],
      );
    });
  }
}
