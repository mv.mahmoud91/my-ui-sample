import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';

class ApiConnection {
  static final String domain = "http://apitest.ecomgroup.ir/";
  static final String baseUrl = "${domain}ClubAPI/";
}

final Dio _dio = Dio();
String apiToken = 'D@rT3b#%\$5_(*467Wd(*46775dfgf#rg\$%fd467';

Future<Dio> getApiConnection() async {
  //  dio instance to request token
//  Dio tokenDio = Dio();
//  token = await getTokenPrefs();
  _dio.interceptors.clear();
  _dio.options.baseUrl = ApiConnection.baseUrl;
  _dio.options.connectTimeout = 60000;
  _dio.options.receiveTimeout = 60000;
  _dio.interceptors.add(LogInterceptor(requestBody: true, responseBody: true));
  _dio.interceptors
      .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
    options.headers["Content-Type"] = "application/json";
    options.headers["Accept"] = "application/json";
    options.headers["Authentication"] = '{"PrivateKey": "$apiToken" }';

    return options;
  }, onResponse: (Response response) {
    return response;
  }));
  return _dio;
}

Future<bool> checkConnectivity() async {
  ConnectivityResult result = await Connectivity().checkConnectivity();
  return result != ConnectivityResult.none;
}
