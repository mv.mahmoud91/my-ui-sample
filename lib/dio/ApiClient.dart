import 'package:dio/dio.dart';
import 'ApiConnection.dart';

Future<Response> callCheckApp(
  String shopes,
  String macAddress,
  String browser,
  String browserVersion,
  String os,
  String osVersion,
  String resolution,
  String ip,
  String apiAddress,
) async {
  Map params = {
    'Shopes': shopes,
    'MacAddress': macAddress,
    'Browser': browser,
    'BrowserVersion': browserVersion,
    'OS': os,
    'OSVersion': osVersion,
    'Resolution': resolution,
    'IP': ip,
  };
  try {
    Dio dio = await getApiConnection();
    Response response = await dio.post("/$apiAddress", data: params);
    return response;
  } catch (error, stacktrace) {
    print("Exception occurred: $error stackTrace: $stacktrace");
    return error;
  }
}

Future<Response> callSignUpMember(
  String macAddress,
  String browser,
  String browserVersion,
  String os,
  String osVersion,
  String resolution,
  String ip,
  String nationalCode,
  String mobile,
  bool isReferal,
  int referalID,
) async {
  Map params = {
    'MacAddress': macAddress,
    'Browser': browser,
    'BrowserVersion': browserVersion,
    'OS': os,
    'OSVersion': osVersion,
    'Resolution': resolution,
    'IP': ip,
    'NationalCode': nationalCode,
    'Mobile': mobile,
    'IsReferal': isReferal,
    'ReferalID': referalID,
  };
  try {
    Dio dio = await getApiConnection();
    Response response = await dio.post("/SetNewMember", data: params);
    return response;
  } catch (error, stacktrace) {
    print("Exception occurred: $error stackTrace: $stacktrace");
    return error;
  }
}

Future<Response> callLogin(
  String macAddress,
  String browser,
  String browserVersion,
  String os,
  String osVersion,
  String resolution,
  String ip,
  int shopID,
  int appID,
  String userName,
  String password,
) async {
  Map params = {
    'MacAddress': macAddress,
    'Browser': browser,
    'BrowserVersion': browserVersion,
    'OS': os,
    'OSVersion': osVersion,
    'Resolution': resolution,
    'IP': ip,
    'ShopeID': shopID,
    'AppID': appID,
  };
  try {
    String authorization = '{"UserName":"$userName","Password":"$password"}';
    String authentication = '{"PrivateKey":"$apiToken"}';
    Options option = Options(headers: {
      "Authentication": authentication,
      "Authorization": authorization,
    });

    Dio dio = await getApiConnection();
    Response response = await dio.post("/Login", data: params, options: option);
    return response;
  } catch (error, stacktrace) {
    print("Exception occurred: $error stackTrace: $stacktrace");
    return error;
  }
}

Future<Response> callRecovery(
  String macAddress,
  String browser,
  String browserVersion,
  String os,
  String osVersion,
  String resolution,
  String ip,
  int shopID,
  int appID,
  String userName,
) async {
  Map params = {
    'MacAddress': macAddress,
    'Browser': browser,
    'BrowserVersion': browserVersion,
    'OS': os,
    'OSVersion': osVersion,
    'Resolution': resolution,
    'IP': ip,
    'ShopeID': shopID,
    'AppID': appID,
  };
  try {
    String authorization = '{"UserName":"$userName"}';
    String authentication = '{"PrivateKey":"$apiToken"}';
    Options option = Options(headers: {
      "Authentication": authentication,
      "Authorization": authorization,
    });

    Dio dio = await getApiConnection();
    Response response =
        await dio.post("/GetPassword", data: params, options: option);
    return response;
  } catch (error, stacktrace) {
    print("Exception occurred: $error stackTrace: $stacktrace");
    return error;
  }
}

Future<Response> callDashBoard(
  String macAddress,
  String browser,
  String browserVersion,
  String os,
  String osVersion,
  String resolution,
  String ip,
  String token,
  String addressApi,
) async {
  Map params = {
    'MacAddress': macAddress,
    'Browser': browser,
    'BrowserVersion': browserVersion,
    'OS': os,
    'OSVersion': osVersion,
    'Resolution': resolution,
    'IP': ip,
  };
  try {
    String authentication = '{"PrivateKey":"$apiToken"}';
    String authorization = '{"Token":"$token"}';
    Options option = Options(headers: {
      "Authentication": authentication,
      "Authorization": authorization,
    });

    Dio dio = await getApiConnection();
    Response response =
        await dio.post("/$addressApi", data: params, options: option);
    return response;
  } catch (error, stacktrace) {
    print("Exception occurred: $error stackTrace: $stacktrace");
    return error;
  }
}

Future<Response> callProfile(
    String macAddress,
    String browser,
    String browserVersion,
    String os,
    String osVersion,
    String resolution,
    String ip,
    String token,
    String addressApi,
    String firsName,
    String lastName,
    String birthDay,
    bool gender) async {
  Map params = {
    'MacAddress': macAddress,
    'Browser': browser,
    'BrowserVersion': browserVersion,
    'OS': os,
    'OSVersion': osVersion,
    'Resolution': resolution,
    'IP': ip,
    'FirstName': firsName,
    'LastName': lastName,
    'BrithDate': birthDay,
    'Gender': gender,
  };
  try {
    String authentication = '{"PrivateKey":"$apiToken"}';
    String authorization = '{"Token":"$token"}';
    Options option = Options(headers: {
      "Authentication": authentication,
      "Authorization": authorization,
    });

    Dio dio = await getApiConnection();
    Response response =
        await dio.post("/$addressApi", data: params, options: option);
    return response;
  } catch (error, stacktrace) {
    print("Exception occurred: $error stackTrace: $stacktrace");
    return error;
  }
}

Future<Response> callPayment(
  String macAddress,
  String browser,
  String browserVersion,
  String os,
  String osVersion,
  String resolution,
  String ip,
  int requestType,
  int terminalId,
  int totalPayment,
  String token,
) async {
  Map params = {
    'MacAddress': macAddress,
    'Browser': browser,
    'BrowserVersion': browserVersion,
    'OS': os,
    'OSVersion': osVersion,
    'Resolution': resolution,
    'IP': ip,
    'RequestType': requestType,
    'TerminalID': terminalId,
    'TotalPayment': totalPayment,
  };
  try {
    String authentication = '{"PrivateKey":"$apiToken"}';
    String authorization = '{"Token":"$token"}';
    Options option = Options(headers: {
      "Authentication": authentication,
      "Authorization": authorization,
    });

    Dio dio = await getApiConnection();
    Response response = await dio.post("/SetPaymentTransactionToken",
        data: params, options: option);
    return response;
  } catch (error, stacktrace) {
    print("Exception occurred: $error stackTrace: $stacktrace");
    return error;
  }
}

Future<Response> callQrCode(
  String macAddress,
  String browser,
  String browserVersion,
  String os,
  String osVersion,
  String resolution,
  String ip,
  String qrCode,
  String token,
) async {
  Map params = {
    'MacAddress': macAddress,
    'Browser': browser,
    'BrowserVersion': browserVersion,
    'OS': os,
    'OSVersion': osVersion,
    'Resolution': resolution,
    'IP': ip,
    'QRcode': qrCode,
  };
  try {
    String authorization = '{"Token":"$token"}';
    String authentication = '{"PrivateKey":"$apiToken"}';
    Options option = Options(headers: {
      "Authentication": authentication,
      "Authorization": authorization,
    });

    Dio dio = await getApiConnection();
    Response response =
        await dio.post("/GetPayment", data: params, options: option);
    return response;
  } catch (error, stacktrace) {
    print("Exception occurred: $error stackTrace: $stacktrace");
    return error;
  }
}

Future<Response> qrPayment(
  String macAddress,
  String browser,
  String browserVersion,
  String os,
  String osVersion,
  String resolution,
  String ip,
  String qrCode,
  double point,
  String cardNo,
  int paymentType,
  int terminalID,
  String token,
) async {
  Map params = {
    'MacAddress': macAddress,
    'Browser': browser,
    'BrowserVersion': browserVersion,
    'OS': os,
    'OSVersion': osVersion,
    'Resolution': resolution,
    'IP': ip,
    'QRcode': qrCode,
    'Point': point,
    'CardNo': cardNo,
    'PaymentType': paymentType,
    'TerminalID': terminalID,
  };
  try {
    String authentication = '{"PrivateKey":"$apiToken"}';
    String authorization = '{"Token":"$token"}';
    Options option = Options(headers: {
      "Authentication": authentication,
      "Authorization": authorization,
    });

    Dio dio = await getApiConnection();
    Response response =
        await dio.post("/SetPayment", data: params, options: option);
    return response;
  } catch (error, stacktrace) {
    print("Exception occurred: $error stackTrace: $stacktrace");
    return error;
  }
}
